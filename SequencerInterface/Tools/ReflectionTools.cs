﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace SequencerInterface.Tools
{
    public static class ReflectionTools
    {
        public static List<PropertyInfo> GetListOfPoperties(Type objType)
        {
            List<PropertyInfo> props = new List<PropertyInfo>();
            foreach (var prop in objType.GetProperties())
                props.Add(prop);

            return props;
        }


        public static bool WriteNestedPropertyValue(Object source, String path, Object value)
        {
            if (path == null || source == null)
                return false;

            Object objNew = source;
            Object objOld = source;
            PropertyInfo thePI = null;

            foreach (String part in path.Split('.'))
            {
                Type type = objNew.GetType();
                if (part.Contains('['))
                {
                    int index1 = part.IndexOf('[');
                    int index2 = part.IndexOf(']');
                    String arrayIndexString = part.Substring(index1 + 1, index2 - (index1 + 1));
                    int arrayIndex = Int32.Parse(arrayIndexString);

                    string arrayName = part.Substring(0, index1);
                    _PropertyInfo info = type.GetProperty(arrayName);
                    var collection = info.GetValue(objNew, null);

                    // note that there's no checking here that the object really
                    // is a collection and thus really has the attribute
                    String indexerName = ((DefaultMemberAttribute)collection.GetType()
                        .GetCustomAttributes(typeof(DefaultMemberAttribute),
                         true)[0]).MemberName;

                    thePI = collection.GetType().GetProperty(indexerName);
                    objOld = objNew;
                    objNew = thePI.GetValue(collection, new object[] { arrayIndex });
                }
                else
                {
                    thePI = type.GetProperty(part);
                    if (thePI == null)
                        return false;
                    objOld = objNew;
                    objNew = thePI.GetValue(objOld, null);
                }
            }

            if (thePI != null)
            {
                if (thePI.PropertyType.IsAssignableFrom(typeof(String)))
                    thePI.SetValue(objOld, value.ToString(), null);
                else
                    thePI.SetValue(objOld, value, null);
                return true;
            }
            return false;
        }

        public static object GetNestedPropertyValue(Object obj, String path)
        {
            if (path == null || obj == null)
                return null;

            foreach (String part in path.Split('.'))
            {
                Type type = obj.GetType();
                if (part.Contains('['))
                {
                    int index1 = part.IndexOf('[');
                    int index2 = part.IndexOf(']');
                    String arrayIndexString = part.Substring(index1 + 1, index2 - (index1 + 1));
                    int arrayIndex = Int32.Parse(arrayIndexString);

                    string arrayName = part.Substring(0, index1);
                    _PropertyInfo info = type.GetProperty(arrayName);
                    var collection = info.GetValue(obj, null);

                    // note that there's no checking here that the object really
                    // is a collection and thus really has the attribute
                    String indexerName = ((DefaultMemberAttribute)collection.GetType()
                        .GetCustomAttributes(typeof(DefaultMemberAttribute),
                         true)[0]).MemberName;

                    _PropertyInfo thePI = collection.GetType().GetProperty(indexerName);
                    obj = thePI.GetValue(collection, new object[] { arrayIndex });
                }
                else
                {
                    _PropertyInfo thePI = type.GetProperty(part);
                    if (thePI == null)
                        return false;
                    obj = thePI.GetValue(obj, null);
                }
            }
            return obj;
        }
    }
}
