﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System;

namespace SequencerInterface.Tools
{

    [Serializable]
    public class EnumKeyValuePair
    {
        public String Key { get; set; }
        public short Value { get; set; }

        public EnumKeyValuePair() { }
        public EnumKeyValuePair(string key, short value)
        {
            Key = key;
            Value = value;
        }
    }

    [Serializable]
    public class EnumKeyDoublePair
    {
        public String Key { get; set; }
        public double Value { get; set; }

        public EnumKeyDoublePair() { }
        public EnumKeyDoublePair(string key, double value)
        {
            Key = key;
            Value = value;
        }
    }
}
