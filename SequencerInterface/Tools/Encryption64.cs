﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SequencerInterface.Tools
{
    /// <summary>
    ///     Encryption utility class
    /// </summary>
    public static class Encryption64
    {
        private static byte[] key;
        private static readonly byte[] iV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        /// <summary>
        ///     Decrypt the file content string pass in, using the salt provided
        /// </summary>
        /// <param name="stringToDecrypt"></param>
        /// <param name="sEncryptionSalt"></param>
        /// <returns></returns>
        public static string Decrypt(string stringToDecrypt, string sEncryptionSalt)
        {
            key = System.Text.Encoding.UTF8.GetBytes(sEncryptionSalt.Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Convert.FromBase64String(stringToDecrypt);

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, iV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();

            System.Text.Encoding encoding = System.Text.Encoding.UTF8;

            return encoding.GetString(ms.ToArray());
        }

        /// <summary>
        ///     Encrypt the file content string pass in, using the salt provided
        /// </summary>
        /// <param name="stringToEncrypt"></param>
        /// <param name="sEncryptionSalt"></param>
        /// <returns></returns>
        public static string Encrypt(string stringToEncrypt, string sEncryptionSalt)
        {
            key = System.Text.Encoding.UTF8.GetBytes(sEncryptionSalt.Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();

            return Convert.ToBase64String(ms.ToArray());
        }
    }
}
