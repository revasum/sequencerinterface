﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SequencerInterface.Tools
{
    public static class XmlSerializerTools
    {
        /// <summary>
        ///     This is important to NEVER change!!
        /// </summary>
        private const string SALT = "##8r4!Il";



        public static T Deserializer<T>(string filename)
        {
            string rawFileContent = String.Empty;
            string decryptedFileContent = String.Empty;


            try
            {
                //read whats on file. Encrypted or not
                rawFileContent = File.ReadAllText(filename);
            }
            catch (System.Xml.XmlException ex)
            {
                throw new InvalidOperationException($"{ex.Message} Error accessing/reading {filename}");
            }

            try
            {
                // check if file is XML, it will error out if it was encrypted
                if (rawFileContent.StartsWith("<?xml") && rawFileContent.EndsWith(">"))
                {
                    decryptedFileContent = rawFileContent;
                }
                else
                {
                    decryptedFileContent = Encryption64.Decrypt(rawFileContent, SALT);
                }
            }
            catch (System.Xml.XmlException ex)
            {
                throw new InvalidOperationException($"{ex.Message} Error deserializing {filename}");
            }

            
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(T));

                using (TextReader reader = new StringReader(decryptedFileContent))
                {
                    return (T)deserializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Error deserializing object: " + filename + "\n" + e.Message);
            }
        }

        /// <summary>
        ///    Serialize the object to file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="fileDir"></param>
        /// <param name="encryptFile"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public static void Serializer<T>(T data, string fileDir, bool encryptFile)
        {
            try
            {
                // Depends on having a MainEditorViewModel; Should not be writing without a GUI
                if (encryptFile)
                {
                    using (TextWriter textWriter = new StreamWriter(fileDir))
                    using (StringWriter stringWriter = new StringWriter())
                    {
                        XmlSerializer writer = new XmlSerializer(typeof(T));

                        writer.Serialize(stringWriter, data);
                        textWriter.Write(Encryption64.Encrypt(stringWriter.ToString(), SALT));
                    }
                }
                else
                {
                    using (StreamWriter file = new StreamWriter(fileDir))
                    {
                        XmlSerializer writer = new XmlSerializer(typeof(T));

                        writer.Serialize(file, data);
                    }
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Error writing XML: " + fileDir + ".\n" + e.Message);
            }
        }
    }
}
