﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System.Collections.Concurrent;
using System.IO;
using TwinCAT.Ads;

namespace SequencerInterface.Tools
{
    public static class ADS_WriteHelper
    {
        public static void FillVoids(BinaryWriter writer, int spacing)
        {
            long initPosition = writer.BaseStream.Position;
            if ((initPosition % spacing) == 0)
                return;
            for (int i = 0; i < (spacing - initPosition % spacing); i++)
                writer.Write((byte)0x00);
        }

        public static ConcurrentDictionary<string, int> VariableHandles = new ConcurrentDictionary<string, int>();

        public static int GetADS_TC_VariableHandle(string variableID, TcAdsClient tcClient)
        {
            if (!VariableHandles.TryGetValue(variableID, out _))
            {
                // Create the handle dictionary
                VariableHandles.GetOrAdd(variableID, tcClient.CreateVariableHandle(variableID));
            }

            VariableHandles.TryGetValue(variableID, out int handle);
            return handle;
        }

        public static void ClearHandles()
        {
            VariableHandles.Clear(); 
        }
    }
}
