﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using SequencerInterface.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SequencerInterface.Tools
{
    public class SimpleTests
    {
        public void TestEnumSave()
        {
            EnumDefinitions test = new EnumDefinitions
            {
                Commands = new ObservableCollection<EnumKeyValuePair>(),
                Objects = new ObservableCollection<EnumKeyValuePair>(),
                EndPoints = new ObservableCollection<EnumKeyValuePair>(),
                SequenceProcessFlag = new ObservableCollection<EnumKeyValuePair>(),
                SequenceStepFlag = new ObservableCollection<EnumKeyValuePair>()
            };

            test.Commands.Add(new EnumKeyValuePair("c1", 1));
            test.Commands.Add(new EnumKeyValuePair("c2", 2));

            test.Objects.Add(new EnumKeyValuePair("o1", 1));
            test.Objects.Add(new EnumKeyValuePair("o2", 2));

            test.EndPoints.Add(new EnumKeyValuePair("e1", 1));
            test.EndPoints.Add(new EnumKeyValuePair("e2", 2));

            test.SequenceProcessFlag.Add(new EnumKeyValuePair("p1", 1));
            test.SequenceProcessFlag.Add(new EnumKeyValuePair("p2", 2));

            test.SequenceStepFlag.Add(new EnumKeyValuePair("s1", 1));
            test.SequenceStepFlag.Add(new EnumKeyValuePair("s2", 2));

            StaticEnumDefinitions.Commands = test.Commands;
            StaticEnumDefinitions.Objects = test.Objects;
            StaticEnumDefinitions.EndPoints = test.EndPoints;
            StaticEnumDefinitions.SequenceProcessFlag = test.SequenceProcessFlag;
            StaticEnumDefinitions.SequenceStepFlag = test.SequenceStepFlag;

            StaticEnumDefinitions.SaveEnumDefintions(@"C:\Users\Chris\Documents\test.xml");
        }
        public void TestPairingsSave()
        {
            ModuleObjectCommandPairings test = new ModuleObjectCommandPairings
            {
                ModuleObjectPairs = new SerializableDictionary<string, List<string>>(),
                ObjectCommandPairs = new SerializableDictionary<string, List<string>>()
            };

            test.ModuleObjectPairs.Add("module1", new List<string>(new String[] { "door", "spindle" }));
            test.ModuleObjectPairs.Add("module2", new List<string>(new String[] { "chuck", "spray" }));


            test.ModuleObjectPairs.Add("door", new List<string>(new String[] { "open", "close" }));
            test.ModuleObjectPairs.Add("spindle", new List<string>(new String[] { "speedUp", "slowDown" }));

            test.ModuleObjectPairs.Add("chuck", new List<string>(new String[] { "speedUp", "slowDown" }));
            test.ModuleObjectPairs.Add("spray", new List<string>(new String[] { "on", "off" }));

            StaticModuleObjectCommandPairings.ModuleObjectPairs = test.ModuleObjectPairs;
            StaticModuleObjectCommandPairings.ObjectCommandPairs = test.ObjectCommandPairs;

            StaticModuleObjectCommandPairings.SaveDefintions(@"C:\Users\Chris\Documents\test2.xml");
        }
    }
}
