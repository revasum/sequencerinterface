﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System;
using System.ComponentModel;

namespace SequencerInterface.Tools
{

    [Serializable]
    public class EnumKeyLocationPair : INotifyPropertyChanged
    {
        private String key;
        private String location;

        public String Key 
        {
            get { return key; }
            set
            {
                if (value == key)
                    return;
                key = value;
                OnPropertyChanged("Key");
            } 
        }
        public String Location
        {
            get { return location; }
            set
            {
                if (value == location)
                    return;
                location = value;
                OnPropertyChanged("Location");
            }
        }

        public EnumKeyLocationPair() { }
        public EnumKeyLocationPair(string key, string location)
        {
            Key = key;
            Location = location;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
