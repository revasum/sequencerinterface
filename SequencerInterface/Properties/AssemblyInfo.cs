﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SequencerInterface")]
[assembly: AssemblyDescription("Revasum SequencerInterface library")]
[assembly: AssemblyConfiguration("")]
// See SetFileMetadata Template for population
//[assembly: AssemblyCompany("Revasum Inc.")]
//[assembly: AssemblyProduct("Tool 6EZ")]
//[assembly: AssemblyCopyright("© Revasum, Inc. 2020.")]
//[assembly: AssemblyTrademark("Revasum is a registered trademark of Revasum Inc. in the U.S.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("773f00e1-9076-49b7-97b3-333a1e7a3142")]

// Version information for an assembly consists of the following four values:
//
//      Major Version 
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// See above for population of data 
//[assembly: AssemblyVersion("1.1.63.0")]
//[assembly: AssemblyFileVersion("1.1.63.0")]
