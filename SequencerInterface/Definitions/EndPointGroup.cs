﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Summary:
 * 
 * Classes:
 * PlcEndPointGroup = mirrored structure on PLC. 
 * EndPointGroup = main class. Bound to GUI. Serialized on sequence table save or load. 
 * 
 * Complexities: 
 * On program startup, Endpoints should be loaded from configuration files. Every EndPointGroup created, either by new or sequence load, 
 * needs copies of these objects. 
 * 
 * */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;

namespace SequencerInterface.Definitions
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public class PlcEndPointGroup
    {
        public short selectedEndPoint;
        public double value1;
    }


    [Serializable]
    public class EndPointGroup : INotifyPropertyChanged
    {
        [System.Xml.Serialization.XmlIgnore()]
        public PlcEndPointGroup PLCEndPointGroup { get; private set; }

        public short SelectedEndPoint
        {
            get
            {
                return PLCEndPointGroup.selectedEndPoint;
            }
            set
            {
                if (PLCEndPointGroup?.selectedEndPoint != value)
                {
                    PLCEndPointGroup.selectedEndPoint = value;
                    OnPropertyChanged("SelectedEndPoint");
                }
            }
        }

        public double Value
        {
            get
            {
                return PLCEndPointGroup.value1;
            }
            set
            {
                if (PLCEndPointGroup?.value1 != value)
                {
                    PLCEndPointGroup.value1 = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        private ObservableCollection<EnumKeyValuePair> endpoints;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Endpoints
        {
            get
            {
                return endpoints;
            }
            set
            {
                if (endpoints != value)
                {
                    endpoints = value;
                    OnPropertyChanged("Endpoints");
                }
            }
        }

        public EndPointGroup()
        {
            PLCEndPointGroup = new PlcEndPointGroup();
            Endpoints = new ObservableCollection<EnumKeyValuePair>();
        }
        public EndPointGroup(ObservableCollection<EnumKeyValuePair> endpoints)
        {
            PLCEndPointGroup = new PlcEndPointGroup();
            Endpoints = endpoints; 
        }

        //public void WriteToStream(BinaryWriter writer)
        //{
        //    writer.Write(SelectedEndPoint);
        //    ADS_WriteHelper.FillVoids(writer, 8);
        //    writer.Write(Value);
        //}

        public string Summary()
        {
            if (SelectedEndPoint != 0)
            {
                try
                {
                    var pair = Endpoints.First(x => x.Value == SelectedEndPoint);
                    if(!String.IsNullOrEmpty(pair.Key))
                    {
                        return /*"Endpoint:    "*/"\t" + pair.Key + ", " + Value;
                    }
                }
                catch (Exception) { }
            }
            return ""; 
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
