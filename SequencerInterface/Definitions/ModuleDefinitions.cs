﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Structure to assist with the handling of Module data
 */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;

namespace SequencerInterface.Definitions
{
    [Serializable]
    public class ModuleDefinitions
    {
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Endpoints { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Objects { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Commands { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> SequenceStepFlags { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> SequenceProcessFlags { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyLocationPair> ObjectCommandCascade { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<AxisDefinition> AxisSetPointDefinitions { get; set; }

        //null bytes for stream
        public int NullBytesAfterTime { get; set; }
        public int NullBytesAfterEndpoints { get; set; }
        public int NullBytesAfterCommands { get; set; }
        public int NullBytesAfterAxes { get; set; }

        //amount of each group for this module
        public int NumberOfSteps { get; set; }
        public int NumberOfEndpointGroups { get; set; }
        public int NumberOfCommandGroups { get; set; }
        public int NumberOfAxes { get; set; }
        public int NumberOfStepFlags { get; set; }
        public int NumberOfProcessFlags { get; set; }
    }
}
