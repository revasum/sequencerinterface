﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Class: SequenceTableStructure
 * 
 * Highest level structure for sequence table.
 * Contains Name of table and list of sequence steps.
 * This class is (de)serialized for sequence table saving.
 * Contains some basic functions for GUI support.
 * Needs a copy of ModuleDefinitions.
 * Contains method to write structure to PLC.
 */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using TwinCAT.Ads;

namespace SequencerInterface.Definitions
{
    public enum TransmissionProtocolMode
    { 
        /// <summary>
        ///     Legacy Mode: each element will be transmitted one element at a time
        /// </summary>
        Individual, 
        /// <summary>
        ///     New Mode: more efficient because table will be 
        /// </summary>
        Block 
    };


    public class SequenceTableStructure
    {
        /// <summary>
        ///     This value should be the equivalent value in the PLC's version SEQUENCE_TABLE_NAME_SIZE
        /// </summary>
        public const int SEQUENCE_TABLE_NAME_SIZE = 255;



        public string Name { get; set; }




        /// <summary>
        ///     The list of sequence table steps
        /// </summary>
        public ObservableCollection<TableRecord> SequenceSteps 
        {
            get
            {
                return _sequenceSteps;
            }
            set
            {
                _sequenceSteps = value;
            }
        }
        private ObservableCollection<TableRecord> _sequenceSteps;

        /// <summary>
        ///     The type of module that this sequence table is defined for
        /// </summary>
        public string ModuleTypeName 
        { 
            get; 
            set; 
        }


        // /// <summary>
        // ///     The currently selected sequence step
        // /// </summary>
        //[System.Xml.Serialization.XmlIgnore()]
        //public TableRecord SelectedItem { get; set; }

        private ModuleDefinitions definitions;

        /// <summary> 
        ///     List of sequence step definitions
        /// </summary>
        [System.Xml.Serialization.XmlIgnore()]
        public ModuleDefinitions Definitions 
        {
            get 
            { 
                return definitions; 
            }
            set
            {
                if (definitions != value)
                {
                    definitions = value;

                    try
                    {
                        foreach (var step in _sequenceSteps)
                        {
                            step.Definitions = definitions;
                        }

                        _sequenceSteps.CollectionChanged += SequenceSteps_CollectionChanged;
                    }
                    catch (Exception e)
                    {
                        throw new InvalidOperationException("Error applying module specific setup to the sequence table.\n" + e.StackTrace);
                    }
                }
            }
        }


        /// <summary>
        ///     Default ctor  
        /// </summary>
        public SequenceTableStructure() : this(new ModuleDefinitions())
        {
            
        }

        /// <summary>
        ///     Changed to preload entire structure size to permit us to block copy to the PLC instead of ala-carte
        /// </summary>
        /// <param name="definitions"></param>
        public SequenceTableStructure(ModuleDefinitions definitions)
        {
            _sequenceSteps = new ObservableCollection<TableRecord>();

            this.Definitions = definitions;

            Name = String.Empty;

            _sequenceSteps.CollectionChanged += SequenceSteps_CollectionChanged;
        }

        /// <summary>
        ///     Add an empty sequence step to the table
        /// </summary>
        /// <returns></returns>
        public bool AddStep()
        {
            return this.AddStep(new TableRecord(this.Definitions));
        }

        /// <summary>
        ///     Add the passed-in sequence step to the table
        /// </summary>
        /// <returns></returns>
        public bool AddStep(TableRecord tableRecord)
        {
            if (_sequenceSteps.Count < Definitions.NumberOfSteps)
            {
                _sequenceSteps.Add(tableRecord);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///     Insert a new empty sequence step to the table
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool InsertStep(int index)
        {
            return this.InsertStep(index, new TableRecord(this.Definitions));
        }

        /// <summary>
        ///     Insert the passed-in sequence step to the table
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool InsertStep(int index, TableRecord tableRecord)
        {
            if ((index >= 0) && (index < _sequenceSteps.Count) && (_sequenceSteps.Count < Definitions.NumberOfSteps))
            {
                _sequenceSteps.Insert(index, tableRecord);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///     Event triggered when the sequence table structure changes, renumber the steps of the table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SequenceSteps_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            for (int i = 0; i < _sequenceSteps.Count; i++)
            {
                _sequenceSteps[i].StepNumber = i + 1; 
            }
        }

        /// <summary>
        ///     Write the sequence structure to the PLC
        /// </summary>
        /// <param name="tcClient"></param>
        /// <param name="handleToSequenceStructure"></param>
        public void WriteToPlc(TcAdsClient tcClient, string sequenceModuleTableName, TransmissionProtocolMode transmissionProtocolMode)
        {

            switch (transmissionProtocolMode)
            {
                case TransmissionProtocolMode.Individual:
                    int handleSequenceTable = ADS_WriteHelper.GetADS_TC_VariableHandle(sequenceModuleTableName + ".sSequenceTableName", tcClient);

                    AdsStream adsStream = new AdsStream(SEQUENCE_TABLE_NAME_SIZE);
                    BinaryWriter writer = new BinaryWriter(adsStream, System.Text.Encoding.ASCII);


                    if (Name.Length >= SEQUENCE_TABLE_NAME_SIZE)
                    {
                        Name = Name.Substring(0, SEQUENCE_TABLE_NAME_SIZE);
                    }

                    writer.Write(Name.ToCharArray());

                    //add terminating zero
                    writer.Write('\0');


                    //tcClient.Write(handle, adsStream);

                    // send the entire table all at once
                    for (int i = 0; i < _sequenceSteps.Count; i++)
                    {
                        // writes the current step out to the PLC one label at a time
                        _sequenceSteps[i].WriteToPlc(tcClient, sequenceModuleTableName + ".astTableRecord[" + (i + 1) + "]");
                    }

                    tcClient.Write(handleSequenceTable, adsStream);

                    break;

                case TransmissionProtocolMode.Block:
                    var plcSequenceTableStructure = new PlcSequenceTableStructure(this, sequenceModuleTableName);

                    plcSequenceTableStructure.WriteToPlc(tcClient);
                    break;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tcClient"></param>
        /// <param name="handleToSequenceStructure"></param>
        /// <param name="sequenceTable"></param>
        public static void WriteSequenceToPlc(TcAdsClient tcClient, string handleToSequenceStructure, SequenceTableStructure sequenceTable, TransmissionProtocolMode transmissionProtocolMode)
        {
            sequenceTable.WriteToPlc(tcClient, handleToSequenceStructure, transmissionProtocolMode);
        }

        /// <summary>
        ///     
        /// </summary>
        public ICommand DeleteSelectedStepCommand
        {
            get
            {
                if (deleteSelectedStepCommand == null)
                {
                    deleteSelectedStepCommand = new RelayCommand(this.DeleteSelectedStep);
                }

                return deleteSelectedStepCommand;
            }
        }
        private ICommand deleteSelectedStepCommand;

        /// <summary>
        ///     Delete the currently selected step
        /// </summary>
        private void DeleteSelectedStep(object state)
        {
            TableRecord tableRecord = state as TableRecord;

            if ((tableRecord.StepNumber > -1) && (tableRecord.StepNumber <= _sequenceSteps.Count) && (_sequenceSteps.Count > 1))
            {
                _sequenceSteps.RemoveAt(tableRecord.StepNumber - 1);
            }
        }

        /// <summary>
        ///     Add step before the currently selected step
        /// </summary>
        public ICommand AddStepBeforeCommand
        {
            get
            {
                if (_addStepBeforeCommand == null)
                {
                    _addStepBeforeCommand = new RelayCommand(this.AddStepBefore);
                }

                return _addStepBeforeCommand;
            }
        }
        private ICommand _addStepBeforeCommand;

        /// <summary>
        ///     Add the new step Before the currently selected one
        /// </summary>
        private void AddStepBefore(object state)
        {
            TableRecord tableRecord = state as TableRecord;

            if ((tableRecord.StepNumber <= _sequenceSteps.Count) && (_sequenceSteps.Count < Definitions.NumberOfSteps))
            {
                _sequenceSteps.Insert(tableRecord.StepNumber - 1, new TableRecord(Definitions));
            }
        }

        /// <summary>
        ///     Add step After the currently selected step
        /// </summary>
        public ICommand AddStepAfterCommand
        {
            get
            {
                if (_addStepAfterCommand == null)
                {
                    _addStepAfterCommand = new RelayCommand(this.AddStepAfter);
                }

                return _addStepAfterCommand;
            }
        }
        private ICommand _addStepAfterCommand;

        /// <summary>
        ///     Add the new step after the currently selected one
        /// </summary>
        private void AddStepAfter(object state)
        {
            TableRecord tableRecord = state as TableRecord;

            if ((tableRecord.StepNumber <= _sequenceSteps.Count) && (_sequenceSteps.Count < Definitions.NumberOfSteps))
            {
                _sequenceSteps.Insert(tableRecord.StepNumber, new TableRecord(Definitions));
            }
        }

        /// <summary>
        ///     Command to make a copy of the current step
        /// </summary>
        public ICommand CopyStepCommand
        {
            get
            {
                if (copyStepCommand == null)
                {
                    copyStepCommand = new RelayCommand(this.CopyStep);
                }

                return copyStepCommand;
            }
        }
        private ICommand copyStepCommand;


        /// <summary>
        ///     Place a copy of the current step onto the clipboard
        /// </summary>
        private void CopyStep(object state)
        {
            TableRecord tableRecord = state as TableRecord;

            if ((tableRecord.StepNumber > -1) && (tableRecord.StepNumber <= _sequenceSteps.Count))
            {
                copiedStep = tableRecord;
            }
        }
        private TableRecord copiedStep;

        /// <summary>
        ///     Command to paste a copy of the current step
        /// </summary>
        public ICommand PasteBeforeStepCommand
        {
            get
            {
                if (_pasteBeforeStepCommand == null)
                {
                    _pasteBeforeStepCommand = new RelayCommand(this.PasteBeforeStep);
                }

                return _pasteBeforeStepCommand;
            }
        }
        private ICommand _pasteBeforeStepCommand;

        /// <summary>
        ///     Paste a step below the current one
        /// </summary>
        private void PasteBeforeStep(object state)
        {
            TableRecord tableRecord = state as TableRecord;

            if ((tableRecord.StepNumber > -1) && (tableRecord.StepNumber <= _sequenceSteps.Count) && (_sequenceSteps.Count < Definitions.NumberOfSteps))
            {
                if (copiedStep != null)
                {
                    _sequenceSteps.Insert(tableRecord.StepNumber - 1, DeepCopy<TableRecord>.DeepClone<TableRecord>(copiedStep));

                    copiedStep = null;
                }
                else
                {
                    //throw new InvalidOperationException("Clipboard is empty, please copy a Sequence Step before trying to Paste.");
                }
            }
        }

        /// <summary>
        ///     Command to paste a copy of the current step
        /// </summary>
        public ICommand PasteAfterStepCommand 
        {
            get
            {
                if (_pasteAfterStepCommand == null)
                {
                    _pasteAfterStepCommand = new RelayCommand(this.PasteAfterStep);
                }

                return _pasteAfterStepCommand;
            }
        }
        private ICommand _pasteAfterStepCommand;

        /// <summary>
        ///     Paste a step below the current one
        /// </summary>
        private void PasteAfterStep(object state)
        {
            TableRecord tableRecord = state as TableRecord;

            if ((tableRecord.StepNumber > -1) && (tableRecord.StepNumber <= _sequenceSteps.Count) && (_sequenceSteps.Count < Definitions.NumberOfSteps))
            {
                if (copiedStep != null)
                {
                    _sequenceSteps.Insert(tableRecord.StepNumber, DeepCopy<TableRecord>.DeepClone<TableRecord>(copiedStep));

                    copiedStep = null;
                }
                else
                {
                    //throw new InvalidOperationException("Clipboard is empty, please copy a Sequence Step before trying to Paste.");
                }
            }
        }

        /// <summary>
        ///     Get a serialized string of the current table
        /// </summary>
        /// <returns></returns>
        public string Summary()
        {
            StringBuilder result = new StringBuilder();


            foreach(var step in _sequenceSteps)
            {
                result.Append("Step: " + step.StepNumber + "\n");

                result.Append(step.Summary() + "\n\n\n");
            }

            return result.ToString();
        }
    }
}
