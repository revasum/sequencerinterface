﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Old. To be removed
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace SequencerInterface.Definitions
{
    public class ModuleObjectCommandPairings
    {
        public Tools.SerializableDictionary<String, List<String>> ModuleObjectPairs = new Tools.SerializableDictionary<string, List<string>>();
        public Tools.SerializableDictionary<String, List<String>> ObjectCommandPairs = new Tools.SerializableDictionary<string, List<string>>();

        public ModuleObjectCommandPairings() { }

        public ModuleObjectCommandPairings(
            Tools.SerializableDictionary<String, List<String>> ModuleObjectPairs,
            Tools.SerializableDictionary<String, List<String>> ObjectCommandPairs)
        {
            this.ModuleObjectPairs = ModuleObjectPairs;
            this.ObjectCommandPairs = ObjectCommandPairs;
        }
    }

    public static class StaticModuleObjectCommandPairings
    {
        public static Tools.SerializableDictionary<String, List<String>> ModuleObjectPairs = new Tools.SerializableDictionary<string, List<string>>();
        public static Tools.SerializableDictionary<String, List<String>> ObjectCommandPairs = new Tools.SerializableDictionary<string, List<string>>();

        public static void LoadDefintions(string XmlInformation)
        {
            using (StreamReader reader = new StreamReader(XmlInformation))
            {
                foreach (var module in StaticEnumDefinitions.Modules)
                    ModuleObjectPairs.Add(module, new List<string>());
                foreach (var obj in StaticEnumDefinitions.Objects)
                    ObjectCommandPairs.Add(obj.Key, new List<string>());

                XmlSerializer deserializer = new XmlSerializer(typeof(ModuleObjectCommandPairings));
                ModuleObjectCommandPairings defs = (ModuleObjectCommandPairings)deserializer.Deserialize(reader);

                foreach (var pair in defs.ModuleObjectPairs)
                    if (ModuleObjectPairs.ContainsKey(pair.Key))
                        ModuleObjectPairs[pair.Key] = pair.Value;

                foreach (var pair in defs.ObjectCommandPairs)
                    if (ObjectCommandPairs.ContainsKey(pair.Key))
                        ObjectCommandPairs[pair.Key] = pair.Value;
            }
        }

        public static void SaveDefintions(string XmlInformation)
        {
            using (StreamWriter file = new StreamWriter(XmlInformation, false))
            {
                XmlSerializer writer = new XmlSerializer(typeof(ModuleObjectCommandPairings));
                ModuleObjectCommandPairings wrapper = new ModuleObjectCommandPairings(ModuleObjectPairs, ObjectCommandPairs);
                writer.Serialize(file, wrapper);
                file.Close();
            }
        }
    }

}
