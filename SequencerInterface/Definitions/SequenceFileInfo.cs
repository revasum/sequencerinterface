﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SequencerInterface.Definitions
{
    public class SequenceFileInfo
    {
        public SequenceFileInfo( string filenameDirectoryPath, string filename )
        {
            FilenameDirectoryPath = filenameDirectoryPath;
            Filename = filename;
        }

        public string FilenameDirectoryPath
        {
            get; set;
        }

        public string Filename
        {
            get; set;
        }
    }
}
