﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using System;

namespace SequencerInterface.Definitions
{
    public static class Constants
    {
        public const Int16 SequenceObjectID = 100;

        public const Int16 SequencerExecuteID   = 900;
        public const Int16 SequencerPauseID     = 904;
        public const Int16 SequencerResumeID    = 905;
        public const Int16 SequencerAbortID     = 906;
        public const Int16 SequencerStopID      = 907;
        public const Int16 SequencerResetID     = 909;
        public const Int16 SequencerNextStepID  = 910; 

    }

    
}
