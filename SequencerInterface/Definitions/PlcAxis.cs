﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SequencerInterface.Definitions
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public class PlcAxis
    {
        [MarshalAs(UnmanagedType.I2)]
        public Int16 axisID;

        [MarshalAs(UnmanagedType.I2)]
        public Axis.MotionTypes motionType;

        [MarshalAs(UnmanagedType.I2)]
        public Axis.BufferModes bufferMode;

        [MarshalAs(UnmanagedType.I2)]
        public Axis.MotionTableDirection direction;

        public double positionSetPoint;
        public double velocitySetPoint;
        public double accelerationSetPoint;
        public double decelerationSetPoint;
        public double jerkSetPoint;
    }
}
