﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using SequencerInterface.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using TwinCAT.Ads;

/* Summary:
 * 
 * Classes:
 * Module = main module class. 
 *    Serialized for configuration and initialization. (holds file paths for axis/command/object/flags lists. #s of structures)
 *    Bound to GUI. 
 *    Handles ADS connections to PLC module
 *    Holds all instances of module related structures. 
 *    Contains sequence table functions, such as open, save, and send, for the API.
 *    
 * 
 * Complexities: 
 * When Module is being deserialized from file, it should start the deserialization of other config files.
 * The ModulesDefinition object contains lots of module data, and needs to be passed into certain module subsystems. 
 * */

namespace SequencerInterface.Definitions
{
    public class Module //: INotifyPropertyChanged
    {
        #region Constructors
        public Module()
        {
            SequencerModuleDefinitions = new ModuleDefinitions();
        }

        public Module(string currentWorkingDirectory) : this()
        {
            CurrentWorkingDirectory = currentWorkingDirectory;
        }

        public Module(Module rhs)
        {
            SequencerModuleDefinitions = new ModuleDefinitions();

            Name = rhs.Name;
            EndpointsFileLocation = rhs.EndpointsFileLocation;
            ObjectsFileLocation = rhs.ObjectsFileLocation;
            CommandsFileLocation = rhs.CommandsFileLocation;
            SequenceStepFlagFileLocation = rhs.SequenceStepFlagFileLocation;
            ObjectCommandCascadeFileLocation = rhs.ObjectCommandCascadeFileLocation;
            AxisSetPointDefinitionsFileLocation = rhs.AxisSetPointDefinitionsFileLocation;
            CurrentWorkingDirectory = rhs.CurrentWorkingDirectory;
            SequencerObjectID = rhs.SequencerObjectID;
            PlcModuleLocation = rhs.PlcModuleLocation;
            SequenceTableLocation = rhs.SequenceTableLocation;
            SequencerStatusLocation = rhs.SequencerStatusLocation;
            AdsIpAddress = rhs.AdsIpAddress;
            ModuleTypeName = rhs.ModuleTypeName;
            NullBytesAfterTime = rhs.NullBytesAfterTime;
            NullBytesAfterEndpoints = rhs.NullBytesAfterEndpoints;
            NullBytesAfterCommands = rhs.NullBytesAfterCommands;
            NullBytesAfterAxes = rhs.NullBytesAfterAxes;
            NumberOfSteps = rhs.NumberOfSteps;
            NumberOfEndpointGroups = rhs.NumberOfEndpointGroups;
            NumberOfCommandGroups = rhs.NumberOfCommandGroups;
            NumberOfAxes = rhs.NumberOfAxes;
            NumberOfStepFlags = rhs.NumberOfStepFlags;
            NumberOfProcessFlags = rhs.NumberOfProcessFlags;
        }

        public Module(Module rhs, string currentWorkingDirectory) : this(rhs)
        {
            CurrentWorkingDirectory = currentWorkingDirectory;
        }

        #endregion Constructors
        protected string m_SaveFileLocation = null;

        public enum SequencerType : short
        {
            A = 100,
            B = 101
        }

        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<SequencerType> SequencerTypeValues
        {
            get;
            protected set;
        } = Enum.GetValues(typeof(SequencerType)).Cast<SequencerType>();

        public static Module FromXML(string fileLocation)
        {
            return XmlSerializerTools.Deserializer<Module>(fileLocation);
        }

        #region Properties

        protected string name;
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        protected string endpointsFileLocation;
        public virtual string EndpointsFileLocation
        {
            get { return endpointsFileLocation; }
            set
            {
                if (endpointsFileLocation == value) return;
                endpointsFileLocation = value;
                SequencerModuleDefinitions.Endpoints = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
            }
        }
        protected string objectsFileLocation;
        public virtual string ObjectsFileLocation
        {
            get { return objectsFileLocation; }
            set
            {
                if (objectsFileLocation == value) return;
                objectsFileLocation = value;
                SequencerModuleDefinitions.Objects = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
            }
        }
        protected string commandsFileLocation;
        public virtual string CommandsFileLocation
        {
            get { return commandsFileLocation; }
            set
            {
                if (commandsFileLocation == value) return;
                commandsFileLocation = value;
                SequencerModuleDefinitions.Commands = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
            }
        }
        protected string sequenceStepFlagFileLocation;
        public virtual string SequenceStepFlagFileLocation
        {
            get { return sequenceStepFlagFileLocation; }
            set
            {
                if (sequenceStepFlagFileLocation == value) return;
                sequenceStepFlagFileLocation = value;
                SequencerModuleDefinitions.SequenceStepFlags = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
            }
        }
        protected string sequenceProcessFlagFileLocation;
        public virtual string SequenceProcessFlagFileLocation
        {
            get { return sequenceProcessFlagFileLocation; }
            set
            {
                if (sequenceProcessFlagFileLocation == value) return;
                sequenceProcessFlagFileLocation = value;
                SequencerModuleDefinitions.SequenceProcessFlags = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
            }
        }
        protected string objectCommandCascadeFileLocation;
        public virtual string ObjectCommandCascadeFileLocation
        {
            get { return objectCommandCascadeFileLocation; }
            set
            {
                if (objectCommandCascadeFileLocation == value) return;
                objectCommandCascadeFileLocation = value;
                SequencerModuleDefinitions.ObjectCommandCascade = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyLocationPair>>(value);
            }
        }
        protected string axisSetPointDefinitionsFileLocation;
        public virtual string AxisSetPointDefinitionsFileLocation
        {
            get { return axisSetPointDefinitionsFileLocation; }
            set
            {
                if (axisSetPointDefinitionsFileLocation == value) return;
                axisSetPointDefinitionsFileLocation = value;
                SequencerModuleDefinitions.AxisSetPointDefinitions = XmlSerializerTools.Deserializer<ObservableCollection<AxisDefinition>>(value);
            }
        }

        private readonly static string DefaultCurrentWorkingDirectory = @"C:\SequenceEditor\SequenceTables\PTO_Seq";

        public string CurrentWorkingDirectory
        {
            get;
            set;
        } = DefaultCurrentWorkingDirectory;
        #endregion

        #region Sequence table structure and size variables

        protected SequencerType sequencerObjectID = SequencerType.A;
        public virtual SequencerType SequencerObjectID
        {
            get
            {
                return sequencerObjectID;
            }
            set
            {
                sequencerObjectID = value;
            }
        }

        protected string plcModuleLocation = null;
        public virtual string PlcModuleLocation
        {
            get
            {
                return plcModuleLocation;
            }
            set
            {
                plcModuleLocation = value;
            }
        }

        protected string sequenceTableLocation = null;
        public virtual string SequenceTableLocation
        {
            get
            {
                return sequenceTableLocation;
            }
            set
            {
                sequenceTableLocation = value;
            }
        }

        protected string sequencerStatusLocation = null;
        public virtual string SequencerStatusLocation
        {
            get
            {
                return sequencerStatusLocation;
            }
            set
            {
                sequencerStatusLocation = value;
            }
        }

        protected string adsIpAddress = null;
        public virtual string AdsIpAddress
        {
            get
            {
                return adsIpAddress;
            }
            set
            {
                //set if module resides on private PLC
                adsIpAddress = value;
            }
        }

        protected string moduleTypeName = null;
        public virtual string ModuleTypeName // Sequence tables can be shared between modules with the same ModuleTypeName.
        {
            get
            {
                return moduleTypeName;
            }
            set
            {
                moduleTypeName = value;
            }
        }

        public virtual int NullBytesAfterTime 
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterTime;
            }
            set
            {
                SequencerModuleDefinitions.NullBytesAfterTime = value;
            }
        }
        public virtual int NullBytesAfterEndpoints
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterEndpoints;
            }
            set
            {
                SequencerModuleDefinitions.NullBytesAfterEndpoints = value;
            }
        }
        public virtual int NullBytesAfterCommands
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterCommands;
            }
            set
            {
                SequencerModuleDefinitions.NullBytesAfterCommands = value;
            }
        }
        public virtual int NullBytesAfterAxes
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterAxes;
            }
            set
            {
                SequencerModuleDefinitions.NullBytesAfterAxes = value;
            }
        }

        //amount of each group for this module
        public virtual int NumberOfSteps
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfSteps;
            }
            set
            {
                SequencerModuleDefinitions.NumberOfSteps = value;
            }
        }
        public virtual int NumberOfEndpointGroups
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfEndpointGroups;
            }
            set
            {
                SequencerModuleDefinitions.NumberOfEndpointGroups = value;
            }
        }
        public virtual int NumberOfCommandGroups
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfCommandGroups;
            }
            set
            {
                SequencerModuleDefinitions.NumberOfCommandGroups = value;
            }
        }
        public virtual int NumberOfAxes
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfAxes;
            }
            set
            {
                SequencerModuleDefinitions.NumberOfAxes = value;
            }
        }
        public virtual int NumberOfStepFlags
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfStepFlags;
            }
            set
            {
                SequencerModuleDefinitions.NumberOfStepFlags = value;
            }
        }
        public virtual int NumberOfProcessFlags
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfProcessFlags;
            }
            set
            {
                SequencerModuleDefinitions.NumberOfProcessFlags = value;
            }
        }
        #endregion

        protected SequenceTableStructure currentSequenceTable;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual SequenceTableStructure CurrentSequenceTable 
        {
            get { return currentSequenceTable; }
            set
            {
                if (currentSequenceTable == value) return;
                currentSequenceTable = value;
            }
        }

        protected TcAdsClient localCopyTcClient;
        protected CommandHandshakingStyle? handshakeStyle = null;
        protected int handleToSequenceStructure;
        protected int handleToCommandStructure;
        protected int handleToStatus;
        protected int handleToErrorMessage;

        protected bool connected;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual bool Connected
        {
            get { return connected; }
            set 
            {
                if (connected == value) return;
                connected = value;
            }
        }

        //connect using locally specified ads ip address
        public void SimpleConnect(CommandHandshakingStyle handshakeStyle)
        {
            Connected = false;
            this.handshakeStyle = handshakeStyle;

            try
            {
                localCopyTcClient = new TcAdsClient();
                localCopyTcClient.Connect(AdsIpAddress, 851);
                localCopyTcClient.Timeout = 10000; // Set timeout to 10s
            }
            catch (Exception e)
            {
                throw new Exception("Failed to connect to " + Name + "'s sequence table.\n" + e.Message);
            }
            //LiveSequencerStatus = new SequencerStatus();
            //subscribe to sequencer status updates
            try
            {
                handleToSequenceStructure = localCopyTcClient.CreateVariableHandle(SequenceTableLocation);
            }
            catch (Exception)
            {
                throw new Exception("Not connected to the sequence table structure.\n" + SequenceTableLocation);
            }
            try
            {
                handleToCommandStructure = ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".stCommand", localCopyTcClient);
            }
            catch (Exception)
            {
                throw new Exception("Not connected to the module command interface.\n" + PlcModuleLocation + ".stCommand");
            }
            try
            {
                handleToErrorMessage = localCopyTcClient.AddDeviceNotificationEx(PlcModuleLocation + ".sSubsystemErrorMessages", AdsTransMode.Cyclic, 100, 0, null, typeof(string), new int[] { 1024 });
            }
            catch (Exception)
            {
                //optional, chuck exception if a connection can't be made
            }
            Connected = true;
        }

        public void Connect(TcAdsClient tcAdsClient, CommandHandshakingStyle handshakeStyle)
        {
            Connected = false; 
            localCopyTcClient = tcAdsClient;
            this.handshakeStyle = handshakeStyle; 

            //LiveSequencerStatus = new SequencerStatus();
            //subscribe to sequencer status updates
            try
            {
                handleToStatus = tcAdsClient.AddDeviceNotificationEx(SequencerStatusLocation, AdsTransMode.Cyclic, 100, 0, null, typeof(SequencerStatus.AdsSequencerStatus));
            }
            catch (Exception ex1)
            {
                throw new Exception($"{ex1.Message} \nNot connected to sequencer updates for {SequencerStatusLocation}.");
            }
            try
            {
                handleToSequenceStructure = tcAdsClient.CreateVariableHandle(SequenceTableLocation);
            }
            catch (Exception ex2)
            {
                throw new Exception($"{ex2.Message} \nNot connected to the sequence table structure for {SequenceTableLocation}.");
            }
            try
            {
                handleToCommandStructure = ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".stCommand", localCopyTcClient);
            }
            catch (Exception ex3)
            {
                throw new Exception($"{ex3.Message} \nNot connected to the module command interface for {PlcModuleLocation}.stCommand");
            }
            try
            {
                handleToErrorMessage = tcAdsClient.AddDeviceNotificationEx(PlcModuleLocation + ".sSubsystemErrorMessages", AdsTransMode.Cyclic, 100, 0, null, typeof(string), new int[] { 1024 });
            }
            catch (Exception)
            {
                //optional, check exception if a connection can't be made
            }

            tcAdsClient.AdsNotificationEx += new AdsNotificationExEventHandler(StatusUpdate);
            Connected = true; 
        }

        private void StatusUpdate(object sender, AdsNotificationExEventArgs e)
        {
            if (e.NotificationHandle == handleToStatus && e.Value is SequencerStatus.AdsSequencerStatus)
            {
                SequencerStatus.AdsSequencerStatus myGuy = e.Value as SequencerStatus.AdsSequencerStatus;
                LiveSequencerStatus.SequencerState = myGuy.sequencerState;
                LiveSequencerStatus.SequenceTableName = myGuy.sequenceTableName;
                LiveSequencerStatus.TableIndex = myGuy.tableIndex;
                LiveSequencerStatus.ElapsedTimer = myGuy.elapsedTimer;
            }

            if ((e.NotificationHandle == handleToErrorMessage) && (e.Value != null))
            {
                ErrorMessages = e.Value.ToString();
            }
        }

        private uint commandCounter = 0;
        private void WriteCommandToPlc(Int16 objectId, Int16 commandId, string svalue)
        {
            if (!Connected)
            {
                throw new Exception("Sequence Editor Module " + Name + " has not been connected to the PLC.");
            }

            if (this.handshakeStyle == null || this.handshakeStyle == CommandHandshakingStyle.NoneSet)
            {
                throw new Exception("Sequence Editor Module " + Name + " does not have a handshaking style set.");
            }

            try
            {
                PlcCommandGroup commandGroup = new PlcCommandGroup
                {
                    commandNumber = commandCounter,
                    selectedObject = objectId,
                    selectedCommand = commandId,
                    svalue = svalue
                };

                localCopyTcClient.WriteAny(
                    ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".stCommand", localCopyTcClient),
                    commandGroup);

                //6EZ modification to match PTO handshaking
                //write .bStartCommand high
                //wait for .bCommandAccepted high
                //write both .bStartCommand and .bCommandAccepted low
                if (this.handshakeStyle == CommandHandshakingStyle.PTO)
                {
                    localCopyTcClient.WriteAny(
                        ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".bStartCommand", localCopyTcClient),
                        true);

                    int retries = 0;
                    while (!(bool)localCopyTcClient.ReadAny(
                        ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".bCommandAccepted", localCopyTcClient), typeof(bool)))
                    {
                        Thread.Sleep(10);
                        if (retries > 50) //magic number! retry for 500ms.
                        {
                            throw new Exception("Maximum retries exceeded.");
                        }
                        retries++;
                    }

                    localCopyTcClient.WriteAny(
                        ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".bStartCommand", localCopyTcClient),
                        false);
                    localCopyTcClient.WriteAny(
                        ADS_WriteHelper.GetADS_TC_VariableHandle(PlcModuleLocation + ".bCommandAccepted", localCopyTcClient),
                        false);
                }

                if (commandCounter == 0)
                {
                    commandCounter = 1;
                }
                else
                {
                    commandCounter = 0;
                }
            }
            catch(Exception e)
            {
                try
                {
                    throw new InvalidOperationException("Could not send command. " + SequencerModuleDefinitions.Objects.First(x => x.Value == objectId).Key + "; " +
                        SequencerModuleDefinitions.Commands.First(x => x.Value == commandId).Key + "\n" + e.Message);
                }
                catch (Exception e2)
                {
                    throw new InvalidOperationException("Could not send command.\n" + e.Message + " " + e2.Message);
                }
            }
        }

        protected SequencerStatus liveSequencerStatus = new SequencerStatus();
        [System.Xml.Serialization.XmlIgnore()]
        public virtual SequencerStatus LiveSequencerStatus
        {
            get { return liveSequencerStatus; }
            set
            {
                if (liveSequencerStatus == value) return;
                liveSequencerStatus = value;
            }
        }

        protected string errorMessages = String.Empty;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual string ErrorMessages
        {
            get { return errorMessages; }
            set
            {
                if (value == errorMessages) return;
                errorMessages = value;
            }
        }

        protected ModuleDefinitions sequencerModuleDefinitions;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual ModuleDefinitions SequencerModuleDefinitions
        {
            get { return sequencerModuleDefinitions; }
            set
            {
                if (sequencerModuleDefinitions == value) return;
                sequencerModuleDefinitions = value;
            }
        }

        #region ST open and save
        private bool ModuleTypeFailsToMatch(SequenceTableStructure table)
        {
            // A pre-existing sequence table may not have ModuleTypeName set.  Accept it.
            if (table.ModuleTypeName == null) return false;

            // Accept any sequence table if the table has no type name.
            if (table.ModuleTypeName.Length == 0) return false;

            // Accept the sequence table if the type name matches.  (Case insensitive.)
            if (table.ModuleTypeName.Equals(ModuleTypeName, StringComparison.CurrentCultureIgnoreCase)) return false;

            // If type name does not match then reject this sequence table.
            return true;
        }
        public SequenceTableStructure NewSequence()
        {
            CurrentSequenceTable = new SequenceTableStructure(SequencerModuleDefinitions)
            {
                ModuleTypeName = ModuleTypeName
            };
            return CurrentSequenceTable;
        }

        #endregion
        public void RunSequence()
        {
            if (!Connected /*|| handleToCommandStructure <= 0 */)
            {
                throw new InvalidOperationException(Name + " is not connected to the PLC or command structure.");
            }
            else
            {
                WriteCommandToPlc((short)SequencerObjectID, Constants.SequencerExecuteID, "");
            }
        }
        public void UploadSequence()
        {
            if (!Connected /* || handleToSequenceStructure <= 0 */)
            {
                throw new InvalidOperationException(Name + " is not connected to the PLC or the sequence structure.");
            }
            else if (CurrentSequenceTable == null)
            {
                throw new InvalidOperationException("Open a sequence table to upload.");
            }
            else
            {
                // this defaults to the legacy mode (individual)
                TransmissionProtocolMode transmissionProtocolMode = TransmissionProtocolMode.Individual;

                // is the checkbox "UseBlockTransfer" on main form checked
                if (Defaults.DefaultSettings.UseBlockTransfer)
                {
                    transmissionProtocolMode = TransmissionProtocolMode.Block;
                }

                try
                {
                    CurrentSequenceTable.WriteToPlc(localCopyTcClient, SequenceTableLocation, transmissionProtocolMode);
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException("Could not upload sequence table. " + "\n" + e.Message);
                }
            }
        }

        /// <summary>
        ///     Caution, this new argument list is going to break the contract and become incompatible (we could overload it, but I would rather see who all is using this)
        /// </summary>
        /// <param name="table"></param>
        /// <param name="transmissionProtocolMode"></param>
        /// <param name="errorString"></param>
        public void UploadSequence(SequenceTableStructure table, TransmissionProtocolMode transmissionProtocolMode, out string errorString)
        {
            errorString = null;

            if (!Connected /* || handleToSequenceStructure <= 0 */)
            {
                errorString = Name + " is not connected to the PLC or the sequence structure.";
            }

            if (table == null)
            {
                errorString = " invalid sequence argument. 'table' argument was null.";
            }

            try
            {
                //// this defaults to the legacy mode (individual)
                //TransmissionProtocolMode transmissionProtocolMode = TransmissionProtocolMode.Individual;

                //// is the checkbox "UseBlockTransfer" on main form checked
                //if (Defaults.DefaultSettings.UseBlockTransfer)
                //{
                //    transmissionProtocolMode = TransmissionProtocolMode.Block;
                //}

                table.WriteToPlc(localCopyTcClient, SequenceTableLocation, transmissionProtocolMode);
            }
            catch (Exception e)
            {
                errorString = "Could not upload sequence table. " + "\n" + e.Message;
            }
        }

        public void NextStepSequence()
        {
            if (!Connected /* || handleToCommandStructure <= 0 */)
            {
                throw new InvalidOperationException(Name + " is not connected to the PLC or command structure.");
            }
            WriteCommandToPlc((short)SequencerObjectID, Constants.SequencerNextStepID, "");
        }
        public void AbortSequence()
        {
            if (!Connected /* || handleToCommandStructure <= 0 */)
            {
                throw new InvalidOperationException(Name + " is not connected to the PLC or command structure.");
            }
            WriteCommandToPlc((short)SequencerObjectID, Constants.SequencerAbortID, "");
        }
        public void ResetSequence()
        {
            if (!connected /* || handleToCommandStructure <= 0 */)
            {
                throw new InvalidOperationException(Name + " is not connected to the PLC or command structure.");
            }
            else
            {
                WriteCommandToPlc((short)SequencerObjectID, Constants.SequencerResetID, "");
            }
        }

        public void ExecuteSequence(Machine machine)
        {
            if (machine != null)
            {
                const int TimeoutTime = 30000; // ms

                if (!connected)
                {
                    machine.ConnectToAds();
                }

                ResetSequence();

                // Wait at least 2 seconds before checking for status
                Thread.Sleep(2000);

                WaitForStatus(SequencerStates.NotPrepped, TimeoutTime);
                UploadSequence();
                WaitForStatus(SequencerStates.Prepped, TimeoutTime);
                RunSequence();

            }
            else
            {
                throw new InvalidOperationException("Command Failed because machine is null");
            }
        }

        private void WaitForStatus( SequencerStates sequencerState, int timeoutTime )
        {
            const int StatusWaitTime = 500; // ms

            bool timedOut = false;
            int maxRetries = timeoutTime / StatusWaitTime;
            int retries = 0;


            while ((LiveSequencerStatus.SequencerState != sequencerState) && !timedOut)
            {
                if (!IsSequencerErrorState())
                {
                    ++retries;
                    timedOut = (retries == maxRetries);
                    Thread.Sleep(StatusWaitTime);
                }
                else
                {
                    throw new InvalidOperationException("Command Failed with Status: " + sequencerState.ToString());
                }
            }

            if (timedOut)
            {
                throw new InvalidOperationException("Command Timed Out");
            }
        }

        private bool IsSequencerErrorState()
        {
            return ((LiveSequencerStatus.SequencerState == SequencerStates.SequencerProcessNotOK) ||
                    (LiveSequencerStatus.SequencerState == SequencerStates.SequencerMotionFault) ||
                    (LiveSequencerStatus.SequencerState == SequencerStates.WDTFault) ||
                    (LiveSequencerStatus.SequencerState == SequencerStates.SequencerMotionFault) ||
                    (LiveSequencerStatus.SequencerState == SequencerStates.Abort));
        }

        public SequenceTableStructure OpenSequence(string fileLocation)
        {
            if (SequencerModuleDefinitions != null)
            {
                m_SaveFileLocation = fileLocation;
                CurrentSequenceTable = XmlSerializerTools.Deserializer<SequenceTableStructure>(fileLocation);

                if (CurrentSequenceTable != null)
                {
                    CurrentSequenceTable.Definitions = SequencerModuleDefinitions;
                    if (ModuleTypeFailsToMatch(CurrentSequenceTable))
                    {

                        throw new InvalidOperationException("Open Failed. This sequence table was created for module type '" +
                                                            CurrentSequenceTable.ModuleTypeName +
                                                            "' but you are trying to open it on module type '" +
                                                            ModuleTypeName);
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("Invalid Sequencer Module Definitions");
            }

            return CurrentSequenceTable;
        }

        public void SaveSequence(SequenceTableStructure sequence, string fileLocation)
        {
            if (ModuleTypeName != null)
            {
                if (CurrentSequenceTable != null)
                {
                    CurrentSequenceTable.ModuleTypeName = ModuleTypeName;
                    XmlSerializerTools.Serializer<SequenceTableStructure>(sequence, fileLocation, Defaults.DefaultSettings.EncryptFiles);
                }
                else
                {
                    throw new InvalidOperationException("Invalid Current Sequence Table");
                }
            }
            else
            {
                throw new InvalidOperationException("Invalid Sequencer Module Type Name");
            }
        }
    }
}
