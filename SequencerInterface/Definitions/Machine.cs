﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

using SequencerInterface.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using TwinCAT.Ads;

/* Summary:
 * 
 * Classes:
 * Machine = 
 *    Serialized for configuration and initialization. (holds file paths for modules)
 *    Holds all instances of modules. 
 *    Handles master ADS connection.
 *    
 * */

namespace SequencerInterface.Definitions
{
    /// <summary>
    /// Specify handshaking protocol to follow when sending commands to PLC.
    /// 7AF_II is custom revasum style (increment number in command struct, read incremented number[not even followed by sequence editor])
    /// 6EZ follow PTO's handshake command (set inputs, set start flag, read accepted flag, set start flag low)
    /// </summary>
    public enum CommandHandshakingStyle
    {
        NoneSet,
        Revasum,
        PTO
    }

    public class Machine //: INotifyPropertyChanged
    {

        #region Constructors
        public Machine()
        {
            m_Modules = new ObservableCollection<Module>();
            moduleLocations = new ObservableCollection<EnumKeyLocationPair>();
        }

        public Machine(Machine rhs)
        {
            m_Modules = new ObservableCollection<Module>();
            moduleLocations = new ObservableCollection<EnumKeyLocationPair>();

            Name = rhs.Name;
            AdsIpAddress = rhs.AdsIpAddress;
            CommandHandshakeStyle = rhs.CommandHandshakeStyle;
            ModuleLocations = rhs.ModuleLocations;
        }

        #endregion Constructors

        protected string name;
        public virtual string Name
        {
            get { return name; }
            set
            {
                if (value == name) return;
                name = value;
                //OnPropertyChanged("Name");
            }
        }
        protected string adsIpAddress;
        public virtual string AdsIpAddress
        {
            get { return adsIpAddress; }
            set
            {
                if (value == adsIpAddress) return;
                adsIpAddress = value;
               // OnPropertyChanged("AdsIpAddress");
            }
        }
        
        public CommandHandshakingStyle CommandHandshakeStyle { get; set; }

        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<CommandHandshakingStyle> CommandHandshakeStyleValues
        {
            get;
            protected set;
        } = Enum.GetValues(typeof(CommandHandshakingStyle)).Cast<CommandHandshakingStyle>();

        protected ObservableCollection<EnumKeyLocationPair> moduleLocations;
        public virtual ObservableCollection<EnumKeyLocationPair> ModuleLocations
        {
            get
            {
                return moduleLocations;
            }
            set
            {
                moduleLocations = value;
            }
        }

        protected ObservableCollection<Module> m_Modules;

        //[System.Xml.Serialization.XmlIgnore()]
        //public virtual ObservableCollection<Module> Modules
        //{
        //    get { return m_Modules; }
        //    set
        //    {
        //        if (m_Modules == value) return;
        //        m_Modules = value;
        //        //OnPropertyChanged("Modules");
        //    }
        //}

        public virtual void Init()
        {
            foreach (var modLocation in ModuleLocations)
                m_Modules.Add(XmlSerializerTools.Deserializer<Module>(modLocation.Location));
        }

        protected TcAdsClient adsTcClient = null;
        [System.Xml.Serialization.XmlIgnore()]
        public TcAdsClient AdsTcClient
        {
            private set { adsTcClient = value;}
            get 
            {
                if (adsTcClient == null)
                {
                    adsTcClient = new TcAdsClient();
                    adsTcClient.AdsStateChanged += AdsTcClient_AdsStateChanged;
                    adsTcClient.AmsRouterNotification += AdsTcClient_AmsRouterNotification;
                }
                return adsTcClient;
            }
        }


        protected StateInfo adsState;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual StateInfo AdsState
        {
            get { return adsState; }
            set
            {
                if(adsState.Equals(value)) return;
                adsState = value;
                //OnPropertyChanged("AdsState");
            }
        }
        void AdsTcClient_AdsStateChanged(object sender, AdsStateChangedEventArgs e)
        {
            AdsState = e.State;
        }

        protected AmsRouterState routerState;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual AmsRouterState RouterState
        {
            get { return routerState; }
            set
            {
                if (routerState.Equals(value)) return;
                routerState = value;
                //OnPropertyChanged("RouterState");
            }
        }

        protected bool connected;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual bool Connected
        {
            get { return connected; }
            set 
            {
                if (value == connected) return;
                connected = value;
                //OnPropertyChanged("Connected");
            }
        }

        void AdsTcClient_AmsRouterNotification(object sender, AmsRouterNotificationEventArgs e)
        {
            RouterState = e.State;
        }
        public void ConnectToAds()
        {
            try
            {
                AdsTcClient = new TcAdsClient();
                AdsTcClient.Connect(AdsIpAddress, 851);
            }
            catch(Exception e)
            {
                throw new InvalidOperationException("Failed to connect to the PLC.\n" + e.Message);
            }

            ConnectModules();
        }

        protected virtual void ConnectModules()
        {
            foreach (var module in m_Modules)
            {
                try
                {
                    module.Connect(adsTcClient, this.CommandHandshakeStyle);
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException("Module " + module.Name + " failed to connect.\n" + e.Message);
                }
            }
        }

        public virtual void DisconnectFromAds()
        {
            try
            {
                AdsTcClient.Dispose();
                ADS_WriteHelper.ClearHandles(); 
                Connected = false;
                foreach (var module in m_Modules)
                    module.Connected = false;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Failed to disconnect to the PLC.\n"
                                                    + e.Message);
            }
        }

        protected bool _useBlockTransfer = false;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual bool UseBlockTransfer
        {
            get
            {
                return _useBlockTransfer;
            }
            set
            {
                if (_useBlockTransfer == value)
                    return;
                _useBlockTransfer = value;
                //OnPropertyChanged("UseBlockTransfer");

                Defaults.DefaultSettings.UseBlockTransfer = value;
                Defaults.SaveDefaultSettings();
            }
        }

        protected bool _encryptedFiles = true;
        [System.Xml.Serialization.XmlIgnore()]
        public virtual bool EncryptFiles
        {
            get
            {
                return _encryptedFiles;
            }
            set
            {
                if (_encryptedFiles == value)
                    return;
                _encryptedFiles = value;
                //OnPropertyChanged("EncryptFiles");

                Defaults.DefaultSettings.EncryptFiles = value;
                Defaults.SaveDefaultSettings(); 
            }
        }
    }
}
