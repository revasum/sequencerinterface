﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Summary - The most complex file here! 
 * 
 * Classes:
 * PlcAxis = mirrored structure on PLC. Used for ADS communication.
 * AxisDefinition = used for XML configuration serialization. AxisDefintions used to populate Axis objects with info.
 * Axis = main axis object. Contains info to be passed to PLC. Bound to GUI. Saved to .seq files. 
 * SetpointDefinitions = contains all extraneous information for each setpoint, e.g. min, max, scaling. Contains value. Bound to GUI
 * 
 * Complexities: 
 * AxisDefinitions are loaded on program startup. Each axis created after startup (with new sequence command or load sequence) should have a copy of a definition. 
 * 
 * Only Axis objects are serialized on sequence table save or load. When loaded, the saved Values in the axis object need to be passed to the list of SetpointDefinitions.
 * The values saved can either be from a list of predefined setpoints or just a double value. This needs to be parsed correctly. 
 * 
 * Tools are provided to convert from predefined setpoint lists -> values. 
 * */

using SequencerInterface.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace SequencerInterface.Definitions
{
    [Serializable]
    public class Axis : INotifyPropertyChanged
    {
        #region Enums
        public enum MotionTypes : short { None = 0, Absolute = 1050, Relative, Modulo, Velocity = 1053, Halt = 1003, Enable = 1020, Disable = 1021, Home = 1040, Reset = 1041, SOE = 1042, ZeroPosition = 1093, STOReset = 1054, STOandSOEReset= 1055}
        public enum MotionTableDirection : short { Positive = 1, Shortest, Negative, Current }
        public enum BufferModes : short { Aborting = 0, Buffered }


        #region Lists of enums for Combo box display
        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<MotionTypes> MotionTypeValues
        {
            get;
        } = Enum.GetValues(typeof(MotionTypes)).Cast<MotionTypes>();

        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<MotionTableDirection> MotionTableDirectionValues
        {
            get;
        } = Enum.GetValues(typeof(MotionTableDirection)).Cast<MotionTableDirection>();

        
        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<BufferModes> BufferModeValues
        {
            get;
        } = Enum.GetValues(typeof(BufferModes)).Cast<BufferModes>();
        #endregion
        #endregion

        [System.Xml.Serialization.XmlIgnore()]
        public PlcAxis PLCAxis { private set; get; }

        public Int16 AxisID
        {
            get
            {
                return PLCAxis.axisID;
            }
            set
            {
                if (PLCAxis?.axisID != value)
                {
                    PLCAxis.axisID = value;
                    OnPropertyChanged("AxisID");
                }
            }
        }

        private string axisName;
        public string AxisName 
        {
            get
            {
                return axisName; 
            }
            set
            {
                if (axisName != value)
                {
                    axisName = value;
                    OnPropertyChanged("AxisName");
                }
            }
        }

        public MotionTypes MotionType
        {
            get
            {   
                return PLCAxis.motionType;
            }
            set
            {
                if (PLCAxis?.motionType != value)
                {
                    PLCAxis.motionType = value;
                    OnPropertyChanged("MotionType");
                }
            }
        }

        public BufferModes BufferMode
        {
            get
            {
                return PLCAxis.bufferMode;
            }
            set
            {
                if (PLCAxis?.bufferMode != value)
                {
                    PLCAxis.bufferMode = value;
                    OnPropertyChanged("BufferMode");
                }
            }
        }

        public MotionTableDirection Direction
        {
            get
            {
                return PLCAxis.direction;
            }
            set
            {
                if (PLCAxis?.direction != value)
                {
                    PLCAxis.direction = value;
                    OnPropertyChanged("Direction");
                }
            }
        }

        private ObservableCollection<String> setPointValues;
        public ObservableCollection<String> SetPointValues
        {
            get { return setPointValues; }
            set
            {
                if (setPointValues == value) return;
                setPointValues = value;

                for (int i = 0; i < value.Count; i++)
                    if (SetPoints.Count < i)
                    {
                        var predefineSetpointCheck = SetPoints[i].CheckForPredefinedSetpoint(value[i]);
                        if (predefineSetpointCheck != null)
                            SetPoints[i].Value = predefineSetpointCheck.Value;
                        else
                            SetPoints[i].Value = Double.Parse(value[i], CultureInfo.InvariantCulture);
                    }
                if (SetPoints != null)
                    foreach (SetPointDefinition item in SetPoints)
                        item.PropertyChanged += SetPointDefinition_PropertyChanged;

                OnPropertyChanged("SetPointValues");
            }
        }

        private ObservableCollection<SetPointDefinition> setPoints;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<SetPointDefinition> SetPoints
        {
            get { return setPoints; }
            set
            {
                if (setPoints == value) return;
                setPoints = value;

                SetPoints.CollectionChanged += SetPoints_CollectionChanged;
                foreach (SetPointDefinition item in value)
                    item.PropertyChanged += SetPointDefinition_PropertyChanged;

                if (SetPointValues == null || SetPointValues.Count != SetPoints.Count)
                {
                    setPointValues = new ObservableCollection<string>();
                    for (int i = 0; i < value.Count; i++)
                        setPointValues.Add("0");
                }

                //if setpointvalues is filled (sequence built from xml file)
                //go get them, as the values are filled in before the axis definitions
                for (int i = 0; i < SetPoints.Count; i++)
                {
                    var predefineSetpointCheck = SetPoints[i].CheckForPredefinedSetpoint(setPointValues[i]);
                    if (predefineSetpointCheck != null)
                        SetPoints[i].Value = predefineSetpointCheck.Value;
                    else
                        SetPoints[i].Value = Double.Parse(setPointValues[i], CultureInfo.InvariantCulture);
                }

                OnPropertyChanged("SetPoints");
            }
        }
        public Axis() 
        {
            //SetPoints.CollectionChanged += SetPoints_CollectionChanged;
            //SetPointValues = new List<double>();
            //foreach (var setpoint in SetPoints)
            //    SetPointValues.Add(setpoint.Value);
            PLCAxis = new PlcAxis();
        }
        public Axis(Int16 AxisID, string AxisName, ObservableCollection<SetPointDefinition> setpoints)
        {
            PLCAxis = new PlcAxis();
            this.AxisID = AxisID;
            this.AxisName = AxisName;
            MotionType = MotionTypes.None;
            BufferMode = BufferModes.Aborting;
            Direction = MotionTableDirection.Positive;
            SetupSetpoints(setpoints);
        }

        public void SetupSetpoints(ObservableCollection<SetPointDefinition> setpoints) 
        {
            ObservableCollection<SetPointDefinition> newSetpoints = new ObservableCollection<SetPointDefinition>();
            foreach (var setpoint in setpoints)
            {
                SetPointDefinition newSetpoint = new SetPointDefinition
                {
                    TypeEnum = setpoint.TypeEnum,
                    Maximum = setpoint.Maximum,
                    Minimum = setpoint.Minimum,
                    PredefinedSetPoints = setpoint.PredefinedSetPoints,
                    Scaling = setpoint.Scaling,
                    TimeUnit = setpoint.TimeUnit,
                    Unit = setpoint.Unit
                };

                newSetpoints.Add(newSetpoint);
            }
            SetPoints = newSetpoints;

            if(SetPointValues == null)
                SetPointValues = new ObservableCollection<string>();

            SetPoints.CollectionChanged += SetPoints_CollectionChanged;
        }

        private void SetPoints_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                foreach (SetPointDefinition item in e.NewItems)
                    item.PropertyChanged += SetPointDefinition_PropertyChanged;

            if (e.OldItems != null)
                foreach (SetPointDefinition item in e.OldItems)
                    item.PropertyChanged -= SetPointDefinition_PropertyChanged;
        }
        void SetPointDefinition_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(sender is SetPointDefinition)
            {
                var castedSender = sender as SetPointDefinition;
                int i = 0;
                switch (castedSender.TypeEnum)
                {
                    case SetPointDefinition.MotionSetPointTypes.Position:
                        i = 0;
                        break;
                    case SetPointDefinition.MotionSetPointTypes.Velocity:
                        i = 1;
                        break;
                    case SetPointDefinition.MotionSetPointTypes.Acceleration:
                        i = 2;
                        break;
                    case SetPointDefinition.MotionSetPointTypes.Deceleration:
                        i = 3;
                        break;
                    case SetPointDefinition.MotionSetPointTypes.Jerk:
                        i = 4;
                        break;
                }
                for (int j = SetPointValues.Count; j < SetPoints.Count; j++)
                    SetPointValues.Add("0");

                var predefineSetpointCheck = SetPoints[i].CheckForPredefinedSetpoint(SetPoints[i].Value);
                if (predefineSetpointCheck != null)
                    SetPointValues[i] = predefineSetpointCheck.Key;
                else
                    SetPointValues[i] = SetPoints[i].Value.ToString(CultureInfo.InvariantCulture);
            }
        }

        public PlcAxis ReturnPlcStructure()
        {
            if (SetPoints == null || SetPoints.Count < 1)
            {
                return PLCAxis;
            }

            PLCAxis.positionSetPoint = SetPoints[0].Value * SetPoints[0].Scaling;

            if (SetPoints.Count < 2)
            {
                return PLCAxis;
            }

            PLCAxis.velocitySetPoint = SetPoints[1].Value * SetPoints[1].Scaling;

            if (SetPoints.Count < 3)
            {
                return PLCAxis;
            }

            PLCAxis.accelerationSetPoint = SetPoints[2].Value * SetPoints[2].Scaling;

            if (SetPoints.Count < 4)
            {
                return PLCAxis;
            }

            PLCAxis.decelerationSetPoint = SetPoints[3].Value * SetPoints[3].Scaling;

            if (SetPoints.Count < 5)
            {
                return PLCAxis;
            }

            PLCAxis.jerkSetPoint = SetPoints[4].Value * SetPoints[4].Scaling;

            return PLCAxis;
        }


        public string Summary()
        {
            string sum = "";

            if (MotionType != MotionTypes.None && SetPoints != null && SetPoints.Count >= 2)
            {
                sum += "\t" + AxisName;
                sum += ", " + MotionType.ToString();
                if (MotionType == MotionTypes.Velocity)
                {
                    sum += ", " + Direction.ToString();
                }

                foreach (var setpt in SetPoints)
                {
                    sum += SummaryOfSetpoint( setpt, MotionType );
                }

            }

            return sum;
        }
        private string SummaryAbbreviatedTypeName(SetPointDefinition.MotionSetPointTypes setpointType)
        {
            if (setpointType == SetPointDefinition.MotionSetPointTypes.Acceleration) return "Accel";
            if (setpointType == SetPointDefinition.MotionSetPointTypes.Deceleration) return "Decel";
            if (setpointType == SetPointDefinition.MotionSetPointTypes.Jerk) return "Jerk";
            return setpointType.ToString();
        }
        private string SummaryOfBasicSetpoint(SetPointDefinition setPointDefinition)
        {
            string miniSummary = "";
            bool displayOnlyIfNonZero =
                (setPointDefinition.TypeEnum == SetPointDefinition.MotionSetPointTypes.Acceleration
                || setPointDefinition.TypeEnum == SetPointDefinition.MotionSetPointTypes.Deceleration
                || setPointDefinition.TypeEnum == SetPointDefinition.MotionSetPointTypes.Jerk
                );

            if (displayOnlyIfNonZero)
            {
                if (setPointDefinition.Value == 0.0)
                {
                    return "";
                }

                miniSummary += ", " + setPointDefinition.Value + " " + SummaryAbbreviatedTypeName(setPointDefinition.TypeEnum);
            }
            else if (setPointDefinition.UsesPredefinedSetPoints)
            {
                var setpoint = setPointDefinition.CheckForPredefinedSetpoint(setPointDefinition.Value);

                if (setpoint != null)
                {
                    miniSummary += ", " + setpoint.Key;
                }
            }
            else if (setPointDefinition.TypeEnum == SetPointDefinition.MotionSetPointTypes.Velocity)
            {
                miniSummary += ", " + setPointDefinition.Value + " " + setPointDefinition.Unit + "/" + setPointDefinition.TimeUnit;
            }
            else
            {
                miniSummary += ", " + setPointDefinition.Value + " " + setPointDefinition.Unit;
            }

            return miniSummary;
        }

        private string SummaryOfSetpoint(SetPointDefinition setPointDefinition, MotionTypes motionType)
        {
            switch (motionType)
            {
                case MotionTypes.Enable:
                case MotionTypes.Disable:
                case MotionTypes.Halt:
                case MotionTypes.Reset:
                case MotionTypes.SOE:
                case MotionTypes.ZeroPosition:
                    return "";

                case MotionTypes.Velocity:
                    if (setPointDefinition.TypeEnum == SetPointDefinition.MotionSetPointTypes.Position)
                    {   // Ignore position setpoint for velocity moves
                        return "";
                    }
                    return SummaryOfBasicSetpoint(setPointDefinition);

                case MotionTypes.Absolute:
                case MotionTypes.Home:
                case MotionTypes.Modulo:
                case MotionTypes.Relative:
                    return SummaryOfBasicSetpoint(setPointDefinition);
            }

            return "";
        }
        
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    
    
}
