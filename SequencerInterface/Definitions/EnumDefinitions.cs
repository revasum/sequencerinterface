﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Old and unused. 
 * */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace SequencerInterface.Definitions
{
    public static class StaticEnumDefinitions
    {
        public static ObservableCollection<String> Modules { get; set; }
        public static ObservableCollection<EnumKeyValuePair> EndPoints { get; set; }
        public static ObservableCollection<EnumKeyValuePair> Objects { get; set; }
        public static ObservableCollection<EnumKeyValuePair> Commands { get; set; }
        public static ObservableCollection<EnumKeyValuePair> SequenceStepFlag { get; set; }
        public static ObservableCollection<EnumKeyValuePair> SequenceProcessFlag { get; set; }


        //public static void LoadEnumDefintions1(string xmlInformation, string variableName)
        //{
        //    using (StreamReader reader = new StreamReader(xmlInformation))
        //    {
        //        XmlSerializer deserializer = new XmlSerializer(typeof(ObservableCollection<String>));
        //        ObservableCollection<String> Modules = (ObservableCollection<String>)deserializer.Deserialize(reader);
        //    }
        //}

        public static void LoadEnumDefintions(string xmlInformation)
        {
            using (StreamReader reader = new StreamReader(xmlInformation))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(EnumDefinitions));
                EnumDefinitions defs = (EnumDefinitions)deserializer.Deserialize(reader);

                StaticEnumDefinitions.Modules = defs.Modules;
                StaticEnumDefinitions.EndPoints = defs.EndPoints;
                StaticEnumDefinitions.Objects = defs.Objects;
                StaticEnumDefinitions.Commands = defs.Commands;
                StaticEnumDefinitions.SequenceStepFlag = defs.SequenceStepFlag;
                StaticEnumDefinitions.SequenceProcessFlag = defs.SequenceProcessFlag;
            }
        }

        public static void SaveEnumDefintions(string XmlInformation)
        {
            using (StreamWriter file = new StreamWriter(XmlInformation,false))
            {
                XmlSerializer writer = new XmlSerializer(typeof(EnumDefinitions));
                EnumDefinitions wrapper = new EnumDefinitions(Modules, EndPoints, Objects, Commands, SequenceStepFlag, SequenceProcessFlag);
                writer.Serialize(file, wrapper);
                file.Close();
            }
        }
    }
    public class EnumDefinitions
    {
        public ObservableCollection<String> Modules { get; set; }
        public ObservableCollection<EnumKeyValuePair> EndPoints { get; set; }
        public ObservableCollection<EnumKeyValuePair> Objects { get; set; }
        public ObservableCollection<EnumKeyValuePair> Commands { get; set; }
        public ObservableCollection<EnumKeyValuePair> SequenceStepFlag { get; set; }
        public ObservableCollection<EnumKeyValuePair> SequenceProcessFlag { get; set; }

        public EnumDefinitions() { }
        public EnumDefinitions(
            ObservableCollection<String> Modules,
            ObservableCollection<EnumKeyValuePair> EndPoints,
            ObservableCollection<EnumKeyValuePair> Objects,
            ObservableCollection<EnumKeyValuePair> Commands,
            ObservableCollection<EnumKeyValuePair> SequenceStepFlag,
            ObservableCollection<EnumKeyValuePair> SequenceProcessFlag)
        {
            this.Modules = Modules;
            this.EndPoints = EndPoints;
            this.Objects = Objects;
            this.Commands = Commands;
            this.SequenceStepFlag = SequenceStepFlag;
            this.SequenceProcessFlag = SequenceProcessFlag;
        }
    }

    public class EnumLocations
    {
        public string Machine { get; set; }
        public string XmlLocation { get; set; }
    }
}
