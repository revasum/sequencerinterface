﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SequencerInterface.Definitions
{
    [Serializable]
    public class AxisDefinition : INotifyPropertyChanged
    {
        public AxisDefinition()
        {
            SetPoints = new ObservableCollection<SetPointDefinition>();
        }

        private Int16 axisID;
        public Int16 AxisID
        {
            get { return axisID; }
            set
            {
                if (axisID == value) return;
                axisID = value;
                OnPropertyChanged("AxisID");
            }
        }

        private string axisName;
        public string AxisName
        {
            get { return axisName; }
            set
            {
                if (axisName == value) return;
                axisName = value;
                OnPropertyChanged("AxisName");
            }
        }
        private ObservableCollection<SetPointDefinition> setPoints;
        public ObservableCollection<SetPointDefinition> SetPoints
        {
            get { return setPoints; }
            set
            {
                if (setPoints == value) return;
                setPoints = value;
                OnPropertyChanged("SetPointsDefinitions");
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
