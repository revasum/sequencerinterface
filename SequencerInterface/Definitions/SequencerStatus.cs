﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Matches SequencerStatus on PLC.
 * Used to collect status info
 */

using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

namespace SequencerInterface.Definitions
{
    public enum SequencerStates : short
    {
        SequencerProcessNotOK = -3, 
        SequencerMotionFault = -2, 
        WDTFault = -1, 
        EState_Init = 0,
        Idle, 
        NotPrepped, 
        Prepped, 
        IndexStepInit, 
        IndexStepMonitor, 
        IndexStepCompleted,
        TableComplete, 
        TablesComplete, 
        IndexPaused, 
        TablesClear, 
        Abort
    }
    public class SequencerStatus: INotifyPropertyChanged
    {
        [StructLayout(LayoutKind.Sequential, Pack = 0)]
        public class AdsSequencerStatus
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
            public string sequenceTableName;
            public Int16 tableIndex;
            [MarshalAs(UnmanagedType.I2)]
            public SequencerStates sequencerState;
            public UInt32 elapsedTimer;
        }

        private string sequenceTableName;
        private bool connectedToPlc;
        private Int16 tableIndex;
        private SequencerStates sequencerState;
        private UInt32 elapsedTimer;

        public string SequenceTableName
        {
            get { return sequenceTableName; }
            set
            {
                if (sequenceTableName != value)
                {
                    sequenceTableName = value;
                    OnPropertyChanged("SequenceTableName");
                }
            } 
        }

        public bool ConnectedToPlc 
        { 
            get { return connectedToPlc; } 
            set 
            {
                if (connectedToPlc != value)
                {
                    connectedToPlc = value;
                    OnPropertyChanged("ConnectedToPlc");
                }
            } 
        }
        public Int16 TableIndex
        {
            get { return tableIndex; }
            set
            {
                if (tableIndex != value)
                {
                    tableIndex = value;
                    OnPropertyChanged("TableIndex");
                }
            }
        }
        public SequencerStates SequencerState
        {
            get { return sequencerState; }
            set
            {
                if (sequencerState != value)
                {
                    sequencerState = value;
                    OnPropertyChanged("SequencerState");
                }
            }
        }
        public UInt32 ElapsedTimer
        {
            get { return elapsedTimer; }
            set
            {
                if (elapsedTimer != value)
                {
                    elapsedTimer = value;
                    OnPropertyChanged("ElapsedTimer");
                }
            }
        }

        public void ReadFromStream(BinaryReader reader)
        {
            SequenceTableName = reader.ReadChars(81).ToString();
            ConnectedToPlc = reader.ReadBoolean();
            TableIndex = reader.ReadInt16();
            SequencerState = (SequencerStates)reader.ReadInt16();
            ElapsedTimer = reader.ReadUInt32();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string eventString)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(eventString));
        }
    }
}
