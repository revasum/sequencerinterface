﻿using SequencerInterface.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SequencerInterface.Definitions
{
    [Serializable]
    public class SetPointDefinition : INotifyPropertyChanged
    {
        public enum MotionSetPointTypes : short { Position, Velocity, Acceleration, Deceleration, Jerk }
        public enum Units { None, Rotations, Degrees, Inches, Meters, Millimeters, Micrometers }
        public enum TimeUnits { None, Minute, Second, SecondsSquared, SecondsCubed }


        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<Units> UnitValues
        {
            get;
        } = Enum.GetValues(typeof(Units)).Cast<Units>();

        [System.Xml.Serialization.XmlIgnore()]
        public IEnumerable<TimeUnits> TimeUnitsValues
        {
            get;
        } = Enum.GetValues(typeof(TimeUnits)).Cast<TimeUnits>();

        public MotionSetPointTypes TypeEnum;
        public string Type { get { return TypeEnum.ToString(); } }
        private double value1;

        [System.Xml.Serialization.XmlIgnore()]
        public double Value
        {
            get { return value1; }
            set
            {
                if (value1 == value) return;
                value1 = value;
                OnPropertyChanged("Value");
            }
        }
        public double Scaling { get; set; }
        public double Minimum { get; set; }
        public double Maximum { get; set; }
        public Units Unit { get; set; }
        public TimeUnits TimeUnit { get; set; }

        public bool UsesPredefinedSetPoints
        {
            get { return PredefinedSetPoints.Count > 0; }
        }
        public bool NotUsesPredefinedSetPoints
        {
            get { return PredefinedSetPoints.Count == 0; }
        }
        private ObservableCollection<EnumKeyDoublePair> predefinedSetPoints;
        public ObservableCollection<EnumKeyDoublePair> PredefinedSetPoints
        {
            get { return predefinedSetPoints; }
            set
            {
                if (predefinedSetPoints == value) return;
                predefinedSetPoints = value;
                OnPropertyChanged("PredefinedSetPoints");
            }
        }

        private ICommand addDefinedSetPointCommand;

        [System.Xml.Serialization.XmlIgnore()]
        public ICommand AddDefinedSetPointCommand
        {
            get
            {
                if (addDefinedSetPointCommand == null)
                    addDefinedSetPointCommand = new RelayCommand(param => this.AddDefineSetPoint(), null);
                return addDefinedSetPointCommand;
            }
        }
        private void AddDefineSetPoint()
        {
            PredefinedSetPoints.Add(new EnumKeyDoublePair());
        }

        public EnumKeyDoublePair CheckForPredefinedSetpoint(double value)
        {
            if (PredefinedSetPoints != null && PredefinedSetPoints.Count > 0)
            {
                var match = PredefinedSetPoints.First(x => x.Value == value);
                if (match != null && !String.IsNullOrEmpty(match.Key))
                {
                    return match;
                }
            }
            return null;
        }
        public EnumKeyDoublePair CheckForPredefinedSetpoint(string key)
        {
            if (PredefinedSetPoints != null && PredefinedSetPoints.Count > 0)
            {
                try
                {
                    var match = PredefinedSetPoints.First(x => x.Key == key);
                    if (match != null && !String.IsNullOrEmpty(match.Key))
                        return match;
                }
                catch (Exception) { return null; }
            }
            return null;
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
