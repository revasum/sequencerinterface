﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SequencerInterface.Tools;

namespace SequencerInterface.Definitions
{
    public static class Defaults
    {
        public static bool ShowErrorMessages = false;

        public static string Version = "2.00.2";

        public static string DefaultSequenceEditorDirectory { get { return @"C:\SequenceEditor"; } }

        private static DefaultSettings defaults;
        public static DefaultSettings DefaultSettings
        {
            get
            {
                if (defaults == null)
                    defaults = XmlSerializerTools.Deserializer<DefaultSettings>(DefaultSequenceEditorDirectory + @"\DefaultSettings.xml");
                return defaults;
            }
            set
            {
                defaults = value;
            }
        }
        public static void OpenDefaultSettings()
        {
            DefaultSettings = XmlSerializerTools.Deserializer<DefaultSettings>(DefaultSequenceEditorDirectory + @"\DefaultSettings.xml");
        }
        public static void SaveDefaultSettings()
        {
            XmlSerializerTools.Serializer<DefaultSettings>(DefaultSettings, DefaultSequenceEditorDirectory + @"\DefaultSettings.xml", DefaultSettings.EncryptFiles);
        }
    }
}
