﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Class: TableRecord
 * 
 * A single step of a sequence table.
 * Is (de)serialized for sequence table saves.
 * Holds all lists for end steps/commands/axes.
 * Must have a copy of ModuleDefinitions.
 * Can generate a summary of the step by collecting summaries from all lists.
 *  
 */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using TwinCAT.Ads;

namespace SequencerInterface.Definitions 
{
    [Serializable]
    public class TableRecord : INotifyPropertyChanged
    {
        private int stepNumber;
        public int StepNumber
        {
            get { return stepNumber; }
            set
            {
                if (stepNumber == value) return;
                stepNumber = value;
                OnPropertyChanged("StepNumber");
            }
        }

        private string comment;
        public string Comment
        {
            get { return comment; }
            set
            {
                if (comment == value) return;
                comment = value;
                OnPropertyChanged("Comment");
            }
        }

        private string summaryBind;

        [System.Xml.Serialization.XmlIgnore()]
        public string SummaryBind
        {
            get { return summaryBind; }
            set
            {
                if (summaryBind == value) return;
                summaryBind = value;
                OnPropertyChanged("SummaryBind");
            }
        }

        private UInt32 time;
        public UInt32 Time
        {
            get { return time; }
            set
            {
                if (time == value) return;
                time = value;
                OnPropertyChanged("Time");
            }
        }

        public string TimeString
        {
            get
            {
                if (Time == uint.MaxValue)
                {
                    return "NONE";
                }
                else
                {
                    return Time.ToString();
                }
            }
            set
            {
                if (value.ToUpper() == "NONE")
                {
                    Time = uint.MaxValue;
                }
                else
                {
                    Time = uint.Parse(value);
                }
                OnPropertyChanged("TimeString");
            }
        }

        public UInt32 watchDogTime;
        public UInt32 WatchDogTime
        {
            get { return watchDogTime; }
            set
            {
                if (watchDogTime == value) return;
                watchDogTime = value;
                OnPropertyChanged("WatchDogTime");
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public EndPointGroup SelectedEndPoint { get; set; }
        private ObservableCollection<EndPointGroup> endPointGroups;
        public ObservableCollection<EndPointGroup> EndPointGroups
        {
            get { return endPointGroups; }
            set
            {
                if (endPointGroups == value) return;
                endPointGroups = value;
                OnPropertyChanged("EndStepGroups");
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public CommandGroup SelectedCommand { get; set; }
        private ObservableCollection<CommandGroup> commandGroups;
        public ObservableCollection<CommandGroup> CommandGroups
        {
            get { return commandGroups; }
            set
            {
                if (commandGroups == value) return;
                commandGroups = value;
                OnPropertyChanged("CommandGroups");
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public Axis SelectedAxis { get; set; }
        private ObservableCollection<Axis> axisRecords;
        public ObservableCollection<Axis> AxisRecords
        {
            get { return axisRecords; }
            set
            {
                if (axisRecords == value) return;
                axisRecords = value;
                OnPropertyChanged("AxisRecords");
            }
        }

        //following 5 not on PLC
        public Int16 SelectedStepFlag { get; set; }
        private ObservableCollection<FlagGroup> selectedStepFlags;
        public ObservableCollection<FlagGroup> SelectedStepFlags
        {
            get { return selectedStepFlags; }
            set
            {
                if (selectedStepFlags == value) return;
                selectedStepFlags = value;
                OnPropertyChanged("SelectedStepFlags");
            }
        }
        public Int16 SelectedProcessFlag { get; set; }
        private ObservableCollection<Int16> selectedProcessFlags;
        public ObservableCollection<Int16> SelectedProcessFlags
        {
            get { return selectedProcessFlags; }
            set
            {
                if (selectedProcessFlags == value) return;
                selectedProcessFlags = value;
                OnPropertyChanged("SelectedProcessFlags");
            }
        }
        private double estimatedRemoval;
        public double EstimatedRemoval
        {
            get { return estimatedRemoval; }
            set
            {
                if (estimatedRemoval == value) return;
                estimatedRemoval = value;
                OnPropertyChanged("EstimatedRemoval");
            }
        }
        private double estimatedTotalWaferThickness;
        public double EstimatedTotalWaferThickness
        {
            get { return estimatedTotalWaferThickness; }
            set
            {
                if (estimatedTotalWaferThickness == value) return;
                estimatedTotalWaferThickness = value;
                OnPropertyChanged("EstimatedTotalWaferThickness");
            }
        }
        private double estimatedTotalStackThickness;
        public double EstimatedTotalStackThickness
        {
            get { return estimatedTotalStackThickness; }
            set
            {
                if (estimatedTotalStackThickness == value) return;
                estimatedTotalStackThickness = value;
                OnPropertyChanged("EstimatedTotalStackThickness");
            }
        }
        public TableRecord()
        { }
        public TableRecord(ModuleDefinitions definitions)
        {
            Comment = "";
            Time = 20;
            WatchDogTime = 30000;
            EndPointGroups = new ObservableCollection<EndPointGroup>();
            CommandGroups = new ObservableCollection<CommandGroup>();
            AxisRecords = new ObservableCollection<Axis>();
            SelectedStepFlags = new ObservableCollection<FlagGroup>();
            //SelectedProcessFlags = new ObservableCollection<FlagGroup>();


            for (int i = 0; i < definitions.NumberOfEndpointGroups; i++)
            {
                EndPointGroups.Add(new EndPointGroup(definitions.Endpoints));
                EndPointGroups[i].PropertyChanged += TableRecord_PropertyChanged;
            }
            for (int i = 0; i < definitions.NumberOfCommandGroups; i++)
            {
                CommandGroups.Add(new CommandGroup(definitions.Objects, definitions.Commands, definitions.ObjectCommandCascade));
                CommandGroups[i].PropertyChanged += TableRecord_PropertyChanged;
            }
            if (definitions.AxisSetPointDefinitions != null)
                for (int i = 0; i < definitions.NumberOfAxes; i++)
                {
                    AxisRecords.Add(new Axis(definitions.AxisSetPointDefinitions[i].AxisID, definitions.AxisSetPointDefinitions[i].AxisName, 
                        definitions.AxisSetPointDefinitions[i].SetPoints));
                    AxisRecords[i].PropertyChanged += TableRecord_PropertyChanged;
                }
            for (int i = 0; i < definitions.NumberOfStepFlags; i++)
            {
                SelectedStepFlags.Add(new FlagGroup(definitions.SequenceStepFlags));
                SelectedStepFlags[i].PropertyChanged += TableRecord_PropertyChanged;
            }
        }

        private void TableRecord_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Task.Factory.StartNew(() => SummaryBind = SimpleSummary());
        }

        public void WriteToPlc(TcAdsClient tcClient, string handleToSequenceStructure)
        {
            tcClient.WriteAny(ADS_WriteHelper.GetADS_TC_VariableHandle(handleToSequenceStructure + ".IndexTime", tcClient), Time);
            tcClient.WriteAny(ADS_WriteHelper.GetADS_TC_VariableHandle(handleToSequenceStructure + ".IndexWatchDogTime", tcClient), WatchDogTime);

            for (int i = 0; i < EndPointGroups.Count(); i++)
            {
                tcClient.WriteAny(ADS_WriteHelper.GetADS_TC_VariableHandle(handleToSequenceStructure + ".astEndSequenceStepGroup[" + (i + 1) + "]", tcClient), EndPointGroups[i].PLCEndPointGroup);
            }

            for (int i = 0; i < CommandGroups.Count(); i++)
            {
                tcClient.WriteAny(ADS_WriteHelper.GetADS_TC_VariableHandle(handleToSequenceStructure + ".astCommandGroup[" + (i + 1) + "]", tcClient), CommandGroups[i].PLCCommandGroup);
            }

            for (int i = 0; i < AxisRecords.Count(); i++)
            {
                tcClient.WriteAny(ADS_WriteHelper.GetADS_TC_VariableHandle(handleToSequenceStructure + ".astAxesRecord[" + (i + 1) + "]", tcClient), AxisRecords[i].ReturnPlcStructure());
            }
        }


        public string Summary()
        {
            string sum = "";
            sum += "\tTime: " + Time;
            sum += "\n\tWatch Dog Time: " + WatchDogTime;
            sum += "\n\t" + Comment;
            sum += "\n" + SimpleSummary();

            return sum; 
        }
        public string SimpleSummary()
        {
            string sum = "";
            string temp1, temp2 = ""; 

            foreach (var group in EndPointGroups)
                if (!String.IsNullOrEmpty(temp1 = group.Summary()))
                    temp2 += "\n" + temp1;
            if (!String.IsNullOrEmpty(temp2))
                sum += "Endpoints:" + temp2;
            temp2 = "";

            foreach (var group in CommandGroups)
                if (!String.IsNullOrEmpty(temp1 = group.Summary().ToString()))
                    temp2 += "\n" + temp1;
            if (!String.IsNullOrEmpty(temp2))
                sum += "\nCommands:" + temp2;
            temp2 = "";

            foreach (var group in AxisRecords)
                if (!String.IsNullOrEmpty(temp1 = group.Summary()))
                    temp2 += "\n" + temp1;
            if (!String.IsNullOrEmpty(temp2))
            {
                if (temp2.Contains("STOReset"))
                {
                    /*If we are doing an STOReset we want the wait time to be a minimum of 2000ms
                      It takes multiple clock cycles to send the command to the drive's safety card,
                      and the drive can't do anything else until it's done */
                    if (Time < 2000)
                    TimeString = "2000"; 
                }
                sum += "\nAxes:" + temp2;
            }
            temp2 = "";

            foreach (var group in SelectedStepFlags)
                if (!String.IsNullOrEmpty(temp1 = group.Summary()))
                    temp2 += "\n" + temp1;
            if (!String.IsNullOrEmpty(temp2))
                sum += "\nFlags:" + temp2;

            return sum;
        }


        private ModuleDefinitions definitions;
        [System.Xml.Serialization.XmlIgnore()]
        public ModuleDefinitions Definitions 
        {
            get { return definitions; }
            set
            {
                if (definitions == value) return;
                definitions = value;

                if (EndPointGroups == null)
                    EndPointGroups = new ObservableCollection<EndPointGroup>();
                if (CommandGroups == null)
                    CommandGroups = new ObservableCollection<CommandGroup>();
                if (AxisRecords == null)
                    AxisRecords = new ObservableCollection<Axis>();
                if (SelectedStepFlags == null)
                    SelectedStepFlags = new ObservableCollection<FlagGroup>();


                while (EndPointGroups.Count < definitions.NumberOfEndpointGroups)
                {
                    EndPointGroups.Add(new EndPointGroup(definitions.Endpoints));
                    EndPointGroups[EndPointGroups.Count - 1].PropertyChanged += TableRecord_PropertyChanged;
                }
                while (EndPointGroups.Count > definitions.NumberOfEndpointGroups)
                    EndPointGroups.RemoveAt(EndPointGroups.Count - 1);

                while (CommandGroups.Count < definitions.NumberOfCommandGroups)
                {
                    CommandGroups.Add(new CommandGroup(definitions.Objects, definitions.Commands, definitions.ObjectCommandCascade));
                    CommandGroups[CommandGroups.Count - 1].PropertyChanged += TableRecord_PropertyChanged;
                }
                while (CommandGroups.Count > definitions.NumberOfCommandGroups)
                    CommandGroups.RemoveAt(CommandGroups.Count - 1);

                while(AxisRecords.Count < definitions.NumberOfAxes)
                {
                    AxisRecords.Add(new Axis(definitions.AxisSetPointDefinitions[AxisRecords.Count].AxisID, definitions.AxisSetPointDefinitions[AxisRecords.Count].AxisName,
                            definitions.AxisSetPointDefinitions[AxisRecords.Count].SetPoints));
                    AxisRecords[AxisRecords.Count - 1].PropertyChanged += TableRecord_PropertyChanged;
                }
                while (AxisRecords.Count > definitions.NumberOfAxes)
                    AxisRecords.RemoveAt(AxisRecords.Count - 1);

                while(SelectedStepFlags.Count < definitions.NumberOfStepFlags)
                {
                    SelectedStepFlags.Add(new FlagGroup(definitions.SequenceStepFlags));
                    SelectedStepFlags[SelectedStepFlags.Count - 1].PropertyChanged += TableRecord_PropertyChanged;
                }
                while (SelectedStepFlags.Count > definitions.NumberOfStepFlags)
                    SelectedStepFlags.RemoveAt(SelectedStepFlags.Count - 1);


                for (int i = 0; i < EndPointGroups.Count; i++)
                {
                    EndPointGroups[i].Endpoints = definitions.Endpoints;
                    EndPointGroups[i].PropertyChanged += TableRecord_PropertyChanged;
                }
                for (int i = 0; i < CommandGroups.Count; i++)
                {
                    CommandGroups[i].Objects = definitions.Objects;
                    CommandGroups[i].Commands = definitions.Commands;
                    CommandGroups[i].ObjectCommandCascade = definitions.ObjectCommandCascade;
                    CommandGroups[i].PropertyChanged += TableRecord_PropertyChanged;
                }
                for (int i = 0; i < AxisRecords.Count; i++)
                {
                    AxisRecords[i].AxisID = definitions.AxisSetPointDefinitions[i].AxisID;
                    AxisRecords[i].AxisName = definitions.AxisSetPointDefinitions[i].AxisName;
                    AxisRecords[i].SetupSetpoints(definitions.AxisSetPointDefinitions[i].SetPoints);
                    AxisRecords[i].PropertyChanged += TableRecord_PropertyChanged;
                }
                for (int i = 0; i < SelectedStepFlags.Count; i++)
                {
                    SelectedStepFlags[i].Flags = definitions.SequenceStepFlags;
                    SelectedStepFlags[i].PropertyChanged += TableRecord_PropertyChanged;
                }

                //foreach (var group in AxisRecords)
                Task.Factory.StartNew(() => SummaryBind = SimpleSummary());
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
