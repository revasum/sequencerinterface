﻿using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices;
using TwinCAT.Ads;

namespace SequencerInterface.Definitions
{
    /// <summary>
    ///     This managed code memory size has to be identical to the memory block of the sequence table in the PLC.
    ///     This is to ensure we can write the memory block to the PLC in a single operation, instead of one element at a time.
    ///     The result should be a sub-second transmission of a 50 step sequence table instead 8+ seconds.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public class PlcSequenceTableStructure
    {
        /// <summary>
        ///     These values should be consistent with the constants values found in the PLC's memory (PC <=> PLC = 1-to-1)
        /// </summary>
        public const int MAX_SEQUENCE_TABLE_RECORDS = 50;
        public const int SEQUENCE_TABLE_NAME_SIZE = 255;
        public const int MAX_SEQUENCE_ENDSTEP_GROUPS = 2;
        public const int MAX_SEQUENCE_COMMAND_GROUPS = 5;
        public const int MAX_AXES_GROUPS = 5;


        /// <summary>
        ///     The name identifier for this sequence table
        /// </summary>
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 255)]
        public string Name = String.Empty;


        /// <summary>
        ///     The name of the module table is associated/mapped to
        /// </summary>
        public string SequenceModuleName
        {
            get
            {
                return _sequenceModuleName;
            }
            set
            {
                _sequenceModuleName = value;
            }
        }
        private String _sequenceModuleName;

        /// <summary>
        ///     The name identifier for this sequence table
        /// </summary>
        public ModuleDefinitions Definitions
        {
            get
            {
                return _definitions;
            }
            set
            {
                _definitions = value;
            }
        }
        private ModuleDefinitions _definitions;

        /// <summary>
        ///     The name identifier for this sequence table
        /// </summary>
        public Int32 Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }
        private Int32 _size;

        /// <summary>
        ///     The list of sequence table steps (including padding to the complete size represented and defined down in the PLC)
        /// </summary>
        public ObservableCollection<TableRecord> SequenceSteps
        {
            get
            {
                return _sequenceSteps;
            }
            set
            {
                _sequenceSteps = value;
            }
        }
        private ObservableCollection<TableRecord> _sequenceSteps;




        /// <summary>
        ///     Create a full PLC memory block of the sequence table for transmitting to the PLC
        /// </summary>
        /// <param name="sts"></param>
        /// <param name="sequenceModuleName"></param>
        public PlcSequenceTableStructure(SequenceTableStructure sequenceTableStructure, string sequenceModuleName)
        {
            _size = 0;
            _sequenceSteps = new ObservableCollection<TableRecord>();

            _sequenceModuleName = sequenceModuleName;
            _size += SEQUENCE_TABLE_NAME_SIZE + 1;

            _definitions = sequenceTableStructure.Definitions;

            this.Name = sequenceTableStructure.Name;

            // replicate the steps
            for (int i = 0; i < sequenceTableStructure.SequenceSteps.Count; i++)
            {
                _sequenceSteps.Add(sequenceTableStructure.SequenceSteps[i]);
            }

            // append empty records (pad) the end of the sequence table with empty records so we can bulk write in one transmission
            for (int i = _sequenceSteps.Count; i < MAX_SEQUENCE_TABLE_RECORDS; i++)
            {
                // by appending all the steps stored in the PLC we are zeroing out the memory of all 50 steps
                _sequenceSteps.Add(new TableRecord(_definitions));
            }

            _size += (_sequenceSteps.Count * this.GetStepSize(sequenceTableStructure.Definitions));
        }

        /// <summary>
        ///     Calculate the size of the step
        /// </summary>
        /// <returns></returns>
        private Int32 GetStepSize(ModuleDefinitions definitions)
        {
            Int32 result = 0;


            // Timeout
            result += Marshal.SizeOf(typeof(UInt32));

            // WatchdogTimeout
            result += Marshal.SizeOf(typeof(UInt32)); 

            // PlcEndPointGroup
            result += definitions.NumberOfEndpointGroups * (Marshal.SizeOf(typeof(UInt64)) + Marshal.SizeOf(typeof(Double)));

            // PLCCommandGroup
            result += definitions.NumberOfCommandGroups * (Marshal.SizeOf(typeof(UInt32)) + (Marshal.SizeOf(typeof(Int16)) * 2) + CommandGroup.COMMAND_PARAMETER_NAME_SIZE + 1);

            // PlcAxis
            result += definitions.NumberOfAxes * ((Marshal.SizeOf(typeof(Int16)) * 4) + (Marshal.SizeOf(typeof(Double)) * 5));  

            return result;
        }


        /// <summary>
        ///     Write the sequence structure to the PLC as a single operation
        ///     The old way was to transmit each step, each axis, each command, as a separate Tag Write
        /// </summary>
        /// <param name="tcClient"></param>
        /// <param name="handleToSequenceStructure"></param>
        public void WriteToPlc(TcAdsClient tcClient)  
        {  
            int handle = ADS_WriteHelper.GetADS_TC_VariableHandle($"{_sequenceModuleName}", tcClient);


            // finish this
            AdsStream adsStream = new AdsStream();// this.Size);
            BinaryWriter binaryWriter = new BinaryWriter(adsStream, System.Text.Encoding.ASCII);


            // write out the entire contents of the table to a memory stream
            // start with the table name
            this.WriteTableNameToMemoryStream(binaryWriter);

            // then write each of the table steps
            // this should be all 50 (max) steps. some of them will be empty (as defined during constructor)
            for (int stepNumber = 0; stepNumber < _sequenceSteps.Count; stepNumber++)
            {
                // writes the current step out to the memorystream one step at a time
                this.WriteTableRecordToMemoryStream(binaryWriter, _sequenceSteps[stepNumber]);
            }

            // finally, send the entire table all at once down to the PLC
            tcClient.Write(handle, adsStream);
        }

        /// <summary>
        ///     Write (append) the tablename; including padding to 256 chars to the memory stream destined to the PLC 
        /// </summary>
        /// <param name="binaryWriter"></param>
        private void WriteTableNameToMemoryStream(BinaryWriter binaryWriter) 
        {
            Int32 lengthOfTableName = Math.Min(Name.Length, SEQUENCE_TABLE_NAME_SIZE);


            // write the table name
            binaryWriter.Write(Name.ToCharArray(0, lengthOfTableName));

            // right-pad the string with null characters up to the max size
            while (lengthOfTableName++ <= SEQUENCE_TABLE_NAME_SIZE)
            {
                binaryWriter.Write('\0');
            }
        }

        /// <summary>
        ///     Write (append) the current sequence step to the memory stream destined to the PLC 
        /// </summary>
        /// <param name="binaryWriter"></param>
        private void WriteTableRecordToMemoryStream(BinaryWriter binaryWriter, TableRecord tableRecord)
        {
            byte[] time = BitConverter.GetBytes((UInt32)tableRecord.Time);
            binaryWriter.Write(time);

            byte[] watchDogTime = BitConverter.GetBytes((UInt32)tableRecord.WatchDogTime);
            binaryWriter.Write(watchDogTime);


            // append the PLC End Point Group to the memory stream destined to the PLC 
            foreach (var endPointGroup in tableRecord.EndPointGroups)
            {
                byte[] selectedEndPoint = BitConverter.GetBytes((UInt64)endPointGroup.PLCEndPointGroup.selectedEndPoint);
                binaryWriter.Write(selectedEndPoint);

                byte[] value1 = BitConverter.GetBytes((Double)endPointGroup.PLCEndPointGroup.value1);
                binaryWriter.Write(value1);
            }

            // append the PLC Command Group to the memory stream destined to the PLC 
            foreach (var commandGroup in tableRecord.CommandGroups)
            {
                byte[] commandNumber = BitConverter.GetBytes((UInt32)commandGroup.PLCCommandGroup.commandNumber);
                binaryWriter.Write(commandNumber);

                byte[] selectedObject = BitConverter.GetBytes((Int16)commandGroup.PLCCommandGroup.selectedObject);
                binaryWriter.Write(selectedObject);

                byte[] selectedCommand = BitConverter.GetBytes((Int16)commandGroup.PLCCommandGroup.selectedCommand);
                binaryWriter.Write(selectedCommand);


                // write the command parameter name
                String stringValue = commandGroup.PLCCommandGroup.svalue;
                Int32 lengthOfParameterName = Math.Min(stringValue.Length, CommandGroup.COMMAND_PARAMETER_NAME_SIZE);

                binaryWriter.Write(stringValue.ToCharArray(0, lengthOfParameterName));

                // right-pad the string with null characters up to the max size
                while (lengthOfParameterName++ <= CommandGroup.COMMAND_PARAMETER_NAME_SIZE)
                {
                    binaryWriter.Write('\0');
                }
            }

            // append the Axis Record to the current memory stream
            foreach (var axis in tableRecord.AxisRecords)
            {
                // convert the current object to the PLC memory object 
                PlcAxis plcAxis = axis.ReturnPlcStructure();


                //[MarshalAs(UnmanagedType.I2)]
                byte[] axisID = BitConverter.GetBytes((Int16)plcAxis.axisID);
                binaryWriter.Write(axisID);

                //[MarshalAs(UnmanagedType.I2)]
                byte[] motionType = BitConverter.GetBytes((Int16)plcAxis.motionType);
                binaryWriter.Write(motionType);

                //[MarshalAs(UnmanagedType.I2)]
                byte[] bufferMode = BitConverter.GetBytes((Int16)plcAxis.bufferMode);
                binaryWriter.Write(bufferMode);

                //[MarshalAs(UnmanagedType.I2)]
                byte[] direction = BitConverter.GetBytes((Int16)plcAxis.direction);
                binaryWriter.Write(direction);

                //public double positionSetPoint;
                byte[] positionSetPoint = BitConverter.GetBytes((Double)plcAxis.positionSetPoint);
                binaryWriter.Write(positionSetPoint);

                //public double velocitySetPoint;
                byte[] velocitySetPoint = BitConverter.GetBytes((Double)plcAxis.velocitySetPoint);
                binaryWriter.Write(velocitySetPoint);

                //public double accelerationSetPoint;
                byte[] accelerationSetPoint = BitConverter.GetBytes((Double)plcAxis.accelerationSetPoint);
                binaryWriter.Write(accelerationSetPoint);

                //public double decelerationSetPoint;
                byte[] decelerationSetPoint = BitConverter.GetBytes((Double)plcAxis.decelerationSetPoint);
                binaryWriter.Write(decelerationSetPoint);

                //public double jerkSetPoint;
                byte[] jerkSetPoint = BitConverter.GetBytes((Double)plcAxis.jerkSetPoint);
                binaryWriter.Write(jerkSetPoint);
            }
        }
    }
}








