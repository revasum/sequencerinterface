﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Class for sequence step flags (allows editor users to mark steps with flags)
 * Bound to GUI. Serialized on sequence table save or load. 
 * 
 * On program startup, Flags should be loaded from configuration files. Every FlagGroup created, either by new or sequence load, 
 * needs copies of this object. 
 */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace SequencerInterface.Definitions
{
    [Serializable]
    public class FlagGroup: INotifyPropertyChanged
    {
        private short selectedFlag;
        public short SelectedFlag
        {
            get { return selectedFlag; }
            set
            {
                if (selectedFlag == value) return;
                selectedFlag = value;
                OnPropertyChanged("SelectedEndPoint");
            }
        }

        private ObservableCollection<EnumKeyValuePair> flags;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Flags
        {
            get { return flags; }
            set
            {
                if (flags == value) return;
                flags = value;
                OnPropertyChanged("Flags");
            }
        }

        public FlagGroup()
        {
            Flags = new ObservableCollection<EnumKeyValuePair>();
        }
        public FlagGroup(ObservableCollection<EnumKeyValuePair> flags)
        {
            Flags = flags; 
        }
        
        public string Summary()
        {
            if (SelectedFlag != 0)
            {
                try{
                    var pair = Flags.First(x => x.Value == SelectedFlag);
                    if(!String.IsNullOrEmpty(pair.Key))
                    {
                        return "\t" + pair.Key;
                    }
                }
                catch (Exception) { }
            }
            return ""; 
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
