﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Info required on startup.
 * Paired with DefaultSettings.xml
*/
namespace SequencerInterface.Definitions
{
    
    public class DefaultSettings
    {
        public string MachineName { get; set; }
        public string MachineConfigFile { get; set; }
        public bool EncryptFiles { get; set; }
        public bool UseBlockTransfer { get; set; }
    }
}
