﻿#region Copyright
//
// Copyright(C) Revasum, 2020.
//
// This software contains confidential and trade secret information
// belonging to Revasum. All Rights Reserved.
//
// No part of this software may be reproduced or transmitted in any form
// or by any means, electronic, mechanical, photocopying, recording or
// otherwise, without the prior written consent of Revasum
//
#endregion

/* Summary:
 * 
 * Classes:
 * PlcCommandGroup = mirrored structure on PLC. 
 * CommandGroup = main class. Bound to GUI. Serialized on sequence table save or load. 
 * 
 * Complexities: 
 * On program startup, Object, Command, and ObjectCommandCascade should be loaded from configuration files. Every command group created, either by new or sequence load, 
 * needs copies of these objects. 
 * 
 * */

using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;

namespace SequencerInterface.Definitions
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public class PlcCommandGroup
    {
        public UInt32 commandNumber;
        public short selectedObject;
        public short selectedCommand;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
        public string svalue;
    }

    [Serializable]
    public class CommandGroup : INotifyPropertyChanged
    {
        /// <summary>
        ///  This value should be the equivalent value in the PLC's version COMMAND_PARAMETER_NAME_SIZE
        /// </summary>
        public const int COMMAND_PARAMETER_NAME_SIZE = 255;

        [System.Xml.Serialization.XmlIgnore()]
        public PlcCommandGroup PLCCommandGroup { get; private set; }
        public short SelectedObject 
        {
            get
            {
                return PLCCommandGroup.selectedObject;
            }
            set
            {
                if (PLCCommandGroup?.selectedObject != value)
                {
                    PLCCommandGroup.selectedObject = value;
                    OnPropertyChanged("SelectedObject");

                    CommandItemSource?.Clear();

                    if ((Objects != null) && (Objects.Count > 0) && (PLCCommandGroup != null))
                    {
                        var objectKey = Objects.FirstOrDefault(x => x.Value == PLCCommandGroup.selectedObject).Key;

                        if (objectKey != null)
                        {
                            var cascadeList = objectCommandCascade.Where(x => x.Key == objectKey);

                            if (cascadeList != null)
                            {
                                foreach (var casc in cascadeList)
                                {
                                    CommandItemSource.Add(Commands.First(x => x.Key == casc.Location));
                                }
                            }
                        }

                        //if object is changed, clear selected command, parameter
                        SelectedCommand = 0;
                        SValue = "";
                    }
                }
            }
        }

        public short SelectedCommand
        {
            get
            {
                return PLCCommandGroup.selectedCommand;
            }
            set
            {
                if (PLCCommandGroup?.selectedCommand != value)
                {
                    PLCCommandGroup.selectedCommand = value;
                    OnPropertyChanged("SelectedCommand");
                }
            }
        }

        public string SValue
        {
            get
            {
                return PLCCommandGroup.svalue;
            }
            set
            {
                if (PLCCommandGroup?.svalue != value)
                {
                    if (value.Length >= COMMAND_PARAMETER_NAME_SIZE)
                    {
                        PLCCommandGroup.svalue = value.Substring(0, COMMAND_PARAMETER_NAME_SIZE);
                    }
                    else
                    {
                        PLCCommandGroup.svalue = value;
                    }

                    OnPropertyChanged("SValue");
                }
            }
        }

        private ObservableCollection<EnumKeyValuePair> objects;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Objects
        {
            get { return objects; }
            set
            {
                if (objects == value) return;
                objects = value;
                OnPropertyChanged("Objects");
            }
        }

        private ObservableCollection<EnumKeyValuePair> commands;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> Commands
        {
            get { return commands; }
            set
            {
                if (commands == value) return;
                commands = value;
                OnPropertyChanged("Commands");
            }
        }

        private ObservableCollection<EnumKeyLocationPair> objectCommandCascade;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyLocationPair> ObjectCommandCascade
        {
            get { return objectCommandCascade; }
            set
            {
                if (objectCommandCascade == value) return;
                objectCommandCascade = value;
                OnPropertyChanged("ObjectCommandCascade");

                CommandItemSource?.Clear();

                if ( (Objects != null) && (Objects.Count > 0) )
                {
                    var objectKey = Objects.FirstOrDefault(x => x.Value == SelectedObject)?.Key;

                    if( (objectKey != null) && (objectCommandCascade != null) )
                    {
                        var cascadeList = objectCommandCascade.Where(x => x.Key == objectKey);

                        if (cascadeList != null)
                        {
                            foreach (var casc in cascadeList)
                            {
                                CommandItemSource.Add(Commands.First(x => x.Key == casc.Location));
                            }
                        }
                    }
                }
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<EnumKeyValuePair> CommandItemSource { get; set; }

        public CommandGroup() 
        {
            PLCCommandGroup = new PlcCommandGroup();
            Objects = new ObservableCollection<EnumKeyValuePair>();
            Commands = new ObservableCollection<EnumKeyValuePair>();
            SValue = "";
            ObjectCommandCascade = new ObservableCollection<EnumKeyLocationPair>();
            CommandItemSource = new ObservableCollection<EnumKeyValuePair>();
        }
        public CommandGroup(ObservableCollection<EnumKeyValuePair> objects, ObservableCollection<EnumKeyValuePair> commands,
            ObservableCollection<EnumKeyLocationPair> objectCommandCascade)
        {
            PLCCommandGroup = new PlcCommandGroup();
            Objects = objects;
            Commands = commands;
            SValue = "";
            ObjectCommandCascade = objectCommandCascade;
            CommandItemSource = new ObservableCollection<EnumKeyValuePair>();
        }

        public string Summary()
        {
            string sum = "No objects and / or commands found.";

            if (SelectedObject != 0)
            {
                if (((Objects != null) && (Objects.Count > 0)) &&
                    ((Commands != null) && (Commands.Count > 0)))
                {
                    var objectPair = Objects.FirstOrDefault(x => x.Value == SelectedObject);
                    var commandPair = Commands.FirstOrDefault(x => x.Value == SelectedCommand);

                    if ( (objectPair != null) && (commandPair != null) )
                    {
                        if (!String.IsNullOrEmpty(objectPair.Key) && !String.IsNullOrEmpty(commandPair.Key))
                        {
                            //sum += ("Command:    ");
                            //sum += "\t";
                            sum = (objectPair.Key + " do " + commandPair.Key);
                            if (!String.IsNullOrEmpty(SValue))
                            {
                                sum += (". " + SValue);
                            }
                        }
                    }
                }
            }

            return sum;
        }
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            // raise event passing ourself in as event source and indicate name for property that has changed
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
